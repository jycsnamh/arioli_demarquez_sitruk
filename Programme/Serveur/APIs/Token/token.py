import uuid
from datetime import *
from functools import wraps
import jwt
from flask import Flask, app, json, g, request, jsonify, make_response
from flask_cors import CORS

from firebase.DataBase import *

SECRET_KEY = 'QgsfB27DHdeRHbs54sfHFs'

db = DataBase()

class AppFlask :
    app = None
    def __init__(self,name):
        self.app =Flask(name)
        self.app.config['CORS_HEADERS'] = 'Content-Type'
        self.app.config['SECRET_KEY'] = SECRET_KEY
        CORS(self.app)
    
    def register(self,api,url):
        self.app.register_blueprint(api,url_prefix=url)

    def start(self,p = 5000,d=True):
        self.app.run(host="0.0.0.0",port = p,debug=d)

def json_response(payload, status=200):
 return (json.dumps(payload), status, {'content-type': 'projects_apilication/json'})



def token_required_user(f):
    @wraps(f)
    def decorator(*args, **kwargs):

        token = None

        if 'Authorization' in request.headers:
            token = request.headers['Authorization']
        
        if not token:
            return json_response({'message': 'a valid token is missing'},401)

       
        data = jwt.decode(token, options={"verify_signature": False}, algorithms=["HS256"])
        try:
            if datetime.fromtimestamp(data["exp"])< datetime.utcnow():
                return json_response({'message': 'expiration du token'},401) 
            db.reference("Server/Utilisateurs")
            users = dict(db.get())
            current_user = None
            try:
                current_user = users[db.bFind(users,"idUtilisateur",data["idUtilisateur"])]
            except:
                pass
            if current_user == None :
                return json_response({'message': 'Erreur.'},401)    
        except:
            return json_response({'message': 'token is invalid'},401)

        return f(current_user, *args, **kwargs)
    return decorator


def token_required_eleve(f):
    @wraps(f)
    def decorator(*args, **kwargs):

        token = None

        if 'Authorization' in request.headers:
            token = request.headers['Authorization']
        
        if not token:
            return json_response({'message': 'a valid token is missing'},401)

        try:
            data = jwt.decode(token, options={"verify_signature": False}, algorithms=["HS256"])
            if datetime.fromtimestamp(data["exp"])< datetime.utcnow():
                return json_response({'message': 'expiration du token'},401)   
        except:
            return json_response({'message': 'token is invalid'},401)

        return f({"idClasse":data["idClasse"],"nom":data["nom"]}, *args, **kwargs) 
    return decorator