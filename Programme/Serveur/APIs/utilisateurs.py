from logging import NullHandler
from flask import Flask, json, g, request
from flask_cors import CORS, cross_origin
from flask import Blueprint
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import *
from jwt import encode
from APIs.Token.token import *
from firebase.DataBase import *

utilisateurs_api = Blueprint('utilisateurs_api', __name__,url_prefix="Server/Utilisateurs")
CORS(utilisateurs_api)

db = DataBase()

def json_response(payload, status=200):
 return (json.dumps(payload), status, {'content-type': 'projects_apilication/json'})

@utilisateurs_api.route('/registerUser', methods=['GET', 'POST'])
def signup_user(): 
    
    data = request.args.to_dict(flat=False)  
    
    hashed_password = generate_password_hash(data['password'][0], method='sha256')
    
    db.reference("Server/Utilisateurs/")
    lenElems = 0
    idUser = int(uuid.uuid4())% 1000000
    try:
        lenElems = len(dict(db.get()))
        while db.bFind(db.get(),"idUtilisateur",idUser) != None:
            idUser = int(uuid.uuid4())% 1000000
        db.child("user"+ str(lenElems))
    except:
        db.child("user"+ str(lenElems))
    new_user = {"idUtilisateur": idUser, "nom":data['nom'][0],"prenom" : data["prenom"][0], "pseudo" : data["pseudo"][0], "password":hashed_password}
    db.save(new_user)
    return json_response({'message': 'enregistrement reussi'})
    

@utilisateurs_api.route('/loginUser', methods=['GET', 'POST'])  
def login_user(): 
    auth = request.authorization   
    if not auth or not auth.username or not auth.password:  
        return json_response({'Authentication': 'Basic realm: "login required"'},401)    
    db.reference("Server/Utilisateurs/")
    users = db.get()  
    user =db.bFind(users,"pseudo",auth.username)
    if user != None: 
        if check_password_hash(users[user]["password"], auth.password):  
            token = jwt.encode({'idUtilisateur': users[user]["idUtilisateur"], 'exp' : datetime.utcnow() + timedelta(minutes=30)}, SECRET_KEY,algorithm="HS256")
            return json_response({'token' : token}) #token.decode('UTF-8') 
    return json_response( {'Authentication': 'Basic realm: "login required"'},401)