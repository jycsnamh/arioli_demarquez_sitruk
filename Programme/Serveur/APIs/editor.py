from logging import NullHandler
from flask import Flask, json, g, request
from firebase.DataBase import DataBase
from flask_cors import CORS, cross_origin
from flask import Blueprint
import time

from APIs.Token.token import *

db = DataBase()

editor_api = Blueprint('editor_api', __name__,url_prefix="/Editor")
CORS(editor_api)

# Réponse formatée à renvoyer à l'app
def json_response(payload, status=200):
 return (json.dumps(payload), status, {'content-type': 'projects_apilication/json'})




@editor_api.route("/save",methods=['GET'])
@token_required_user
def save(check):
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Exercices")
    script = data["script"][0]
    keyChamp = data["keyChamp"][0] #script[script.find("idScript=")+len("idScript="):script.find(" ; ")]
    db.child(keyChamp)
    db.update("script",script)
    db.reference("Server/Exercices")
    exos = dict(db.get()).values()
    b = False
    for exo in exos:
        if "script" in exo:
            if exo["script"] == script :
                b = True
                break
    if b :
        return json_response({"etat":True})
    else :
        return json_response({"etat":False})




@editor_api.route("/getScriptsEdit",methods=['GET'])
@token_required_user
def getScriptsEdit(check): 
    db.reference("Server")
    db.child("Exercices")
    scripts = {}
    try:
        i = 0
        exercices = dict(db.get())
        for key,exo in exercices.items():
            if "script" in exo:
                scripts[i] = exo["script"]
                i = i + 1
    except :
        pass
    finally :
        return json_response(scripts)


@editor_api.route("/getKeyChampExerciceEdit",methods=['GET'])
@token_required_user
def getKeyChampEdit(check): 
    data = request.args.to_dict(flat=False)
    idExo = data["idExercice"][0]
    db.reference("Server")
    db.child("Exercices")
    keyChamps = {}
    try:
        exercices = dict(db.get())
        for key,exo in exercices.items():
            if exo["idExercice"] == int(idExo):
                keyChamps["keyChamp"] = key
    except :
        pass
    finally :
        return json_response(keyChamps)


@editor_api.route("/loadEdit",methods=['GET'])
@token_required_user
def loadEdit(check):
    data = request.args.to_dict(flat=False)
    idExo = data['idExo'][0]
    db.reference("Server")
    db.child("Exercices")
    exercices = dict(db.get())
    for key, value in exercices.items():
        if(value["idExercice"] == int(idExo)):
            return json_response(value)

    return json_response({})




@editor_api.route("/getScriptsUse",methods=['GET'])
@token_required_eleve
def getScriptsUse(check): 
    db.reference("Server")
    db.child("Exercices")
    scripts = {}
    try:
        i = 0
        exercices = dict(db.get())
        for key,exo in exercices.items():
            if "script" in exo:
                scripts[i] = exo["script"]
                i = i + 1
    except :
        pass
    finally :
        return json_response(scripts)


@editor_api.route("/getKeyChampExerciceUse",methods=['GET'])
@token_required_eleve
def getKeyChampUse(check): 
    data = request.args.to_dict(flat=False)
    idExo = data["idExercice"][0]
    db.reference("Server")
    db.child("Exercices")
    keyChamps = {}
    try:
        exercices = dict(db.get())
        for key,exo in exercices.items():
            if exo["idExercice"] == int(idExo):
                keyChamps["keyChamp"] = key
    except :
        pass
    finally :
        return json_response(keyChamps)


@editor_api.route("/loadUse",methods=['GET'])
@token_required_eleve
def loadUse(check):
    data = request.args.to_dict(flat=False)
    idExo = data['idExo'][0]
    db.reference("Server")
    db.child("Exercices")
    exercices = dict(db.get())
    for key, value in exercices.items():
        if(value["idExercice"] == int(idExo)):
            return json_response(value)

    return json_response({})