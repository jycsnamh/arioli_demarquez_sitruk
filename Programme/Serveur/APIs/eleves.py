from logging import NullHandler
from flask import Flask, json, g, request
from flask_cors import CORS, cross_origin
from flask import Blueprint
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
from jwt import encode
from APIs.Token.token import *
from firebase.DataBase import *

from APIs.Token.token import *

eleves_api = Blueprint('eleves_api', __name__,url_prefix="/Eleves")
CORS(eleves_api)

def json_response(payload, status=200):
 return (json.dumps(payload), status, {'content-type': 'projects_apilication/json'})


@eleves_api.route('/registerEleve', methods=['GET', 'POST'])
def registerEleve(): 
    
    data = request.args.to_dict(flat=False)  
    
    hashed_password = generate_password_hash(data['code'][0], method='sha256')
    
    db.reference("Server/Eleves")
    lenElems = 0
    idEleve = int(uuid.uuid4())% 1000000
    try:
        lenElems = len(dict(db.get()))
        while db.bFind(db.get(),"idEleve",idEleve) != None:
            idEleve = int(uuid.uuid4())% 1000000
    except:
        pass
    finally:
        db.child("eleve"+ str(lenElems))
    new_user = {"idEleve": idEleve, "nom":data['nom'][0], "idClasse" : data["idClasse"][0], "code":hashed_password}
    db.save(new_user)
    return json_response({'message': 'enregistrement reussi'})


@eleves_api.route('/loginEleve', methods=['GET', 'POST'])  
def login_eleve(): 
    auth = request.authorization   
    if not auth or not auth.username or not auth.password:  
        return json_response({"message":"Veuillez remplir les champs."})    
    db.reference("Server/Classes")
    classes = db.get()  
    classe = db.bFind(classes,"code",auth.password)
    if classe != None :
        eleves = {}
        db.child(classe)
        db.child("eleves")
        eleves = dict(db.get())
        eleve =db.bFind(eleves,"nom",auth.username)
        
        if eleve != None: 

            if classes[classe]["code"] == auth.password:  
                token = jwt.encode({'nom': eleves[eleve]["nom"],"idClasse": classes[classe]["idClasse"], 'exp' : datetime.utcnow() + timedelta(hours=2)}, SECRET_KEY,algorithm="HS256")
                return json_response({'token' : token}) #token.decode('UTF-8')
            else:
                return json_response( {'message': "Mauvais code..."})  
        else :
            return json_response( {'message': "Vous n'êtes pas inscrit dans la classe."})      
    else:
        return json_response( {'message': "Le code ne correspond à aucune classe"})


@eleves_api.route("/getPagesToDo",methods=['GET'])
@token_required_eleve
def getPagesToDo(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    classe = db.bFind(classes,"idClasse",int(check["idClasse"]))
    db.child(classe)
    db.child("pages")
    pagesToDo = {}
    try:
        # recuperer les pages à faire de la classe
        pagesToDo = dict(db.get())
        
        # recuperer les pages
        db.reference("Server")
        db.child("Pages")
        pages = dict(db.get())
        
        # recuperer les exercices 
        # db.reference("Server")
        # db.child("Exercices")
        # exos = dict(db.get())
        try:
            for keyPage,pageToDo in pagesToDo.items():
                db.reference("Server/Pages")
                try :
                    page = db.bFind(pages,"idPage",int(pageToDo["idPage"]))
                    db.child(page)
                    db.child("exercices")
                    exosToDo = dict(db.get())
                except:
                    return json_response({"message":"erreur exos Page"},403)
                # for keyExo,exoToDo in exosToDo.items():

                #     exo = db.bFind(exos,"idExercice",int(exoToDo["idExercice"]))
                #     try :
                #         exosToDo[keyExo]= exos[exo]
                #     except:
                #         return json_response({"message":"erreur find exo"},404)
                pagesToDo[keyPage]["exercices"] = exosToDo
        except :
            return json_response({"message":"erreur up 1"},402)
    except:
        return json_response({"message":"erreur base"},401)
    
    return json_response(pagesToDo)