from logging import NullHandler
from flask import Flask, json, g, request
from flask_cors import CORS, cross_origin
from flask import Blueprint

import random

from APIs.Token.token import *

classes_api = Blueprint('classes_api', __name__,url_prefix="/Classes")
CORS(classes_api)

def json_response(payload, status=200):
 return (json.dumps(payload), status, {'content-type': 'projects_apilication/json'})


@classes_api.route("/creerClasse",methods=['GET'])
@token_required_user
def createClasse(check):
    data = request.args.to_dict(flat=False)  
    db.reference("Server/Classes")
    lenElems = 0
    idClasse = int(uuid.uuid4())% 1000000
    try:
        lenElems = len(dict(db.get()))
        while db.bFind(db.get(),"idClasse",idClasse) != None:
            idClasse = int(uuid.uuid4())% 1000000
        db.child("classe"+ str(lenElems))
    except:
        db.child("classe"+ str(lenElems))
    new_classe = {"idClasse":idClasse, "nom":data['nom'][0], "idUtilisateur":check["idUtilisateur"], "dateCreation" : str(datetime.utcnow()) , "dateSuppression" : str(datetime.utcnow() - timedelta(seconds=1))}
    db.save(new_classe)
    return json_response({'message': 'Classe créée.'})




@classes_api.route("/getClasses",methods=['GET'])
@token_required_user
def getClasses(check): 
    db.reference("Server")
    db.child("Classes")
    classes = {}
    try:
        classes = dict(db.get())
        classes = db.selectWithDateCondition(classes,"dateSuppression","dateCreation")
        classes = db.filtreBy(classes,"idUtilisateur",check["idUtilisateur"])
    finally:
        return json_response(classes)



@classes_api.route("/deleteClasse",methods=['DELETE'])
@token_required_user
def deleteClasse(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    classe = db.bFind(classes,"idClasse",int(data["idClasse"][0]))
    db.child(classe)
    db.update("dateSuppression",str(datetime.utcnow()))

    return json_response({"message" : "Classe supprimée."})

@classes_api.route("/addEleve",methods=['POST'])
@token_required_user
def addEleve(check):
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    db.child(db.bFind(classes,"idClasse",int(data["idClasse"][0])))
    db.child("eleves")
    eleves = {}
    try:
        eleves = dict(db.get())
    except:
        pass
    finally:
        db.child("eleve"+str(len(eleves)))
        db.save({"nom" : data["nomEleve"][0], "dateInscription" : str(datetime.utcnow()), "dateSuppression" : str(datetime.utcnow() - timedelta(seconds=1))})
        return json_response({'message' : "Élève ajouté."})

@classes_api.route("/deleteEleve",methods=['DELETE'])
@token_required_user
def deleteEleve(check):
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    db.child(db.bFind(classes,"idClasse",int(data["idClasse"][0])))
    db.child("eleves")
    eleves = {}
    try:
        eleves = dict(db.get())
    except:
        pass
    finally:
        db.delete(eleves,"nom",data["nom"][0])
        return json_response({"message" : "Élève supprimé."})

@classes_api.route("/getEleves",methods=['GET'])
@token_required_user
def getEleves(check):
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    classe = db.bFind(classes,"idClasse",int(data["idClasse"][0]))
    db.child(classe)
    db.child("eleves")
    eleves = {}
    try:
        eleves = dict(db.get())
    except:
        pass
    finally:
        return json_response(eleves)

@classes_api.route("/genererCode",methods=['GET'])
@token_required_user
def genererCode(check):
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    db.child(db.bFind(classes,"idClasse",int(data["idClasse"][0])))
    alphaNum = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9"]
    code = ""
    for i in range(8):
        code = code + random.choice(alphaNum)
    db.update("code" , code)
    return json_response({"message" : "Code généré."})


@classes_api.route("/getCode",methods=['GET'])
@token_required_user
def getCode(check):
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    db.child(db.bFind(classes,"idClasse",int(data["idClasse"][0])))
    classe = dict(db.get())
    if "code" in classe :
        return json_response({"code" : classe["code"]})
    else:
        return json_response({"code" : ""})


@classes_api.route("/getPages",methods=['GET'])
@token_required_user
def getPages(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    classe = db.bFind(classes,"idClasse",int(data["idClasse"][0]))
    db.child(classe)
    db.child("pages")
    pages = {}
    try:
        pages = dict(db.get())
    except:
        pass
    finally:
        return json_response(pages)

@classes_api.route("/addPage",methods=['POST'])
@token_required_user
def addPage(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    classe = db.bFind(classes,"idClasse",int(data["idClasse"][0]))
    db.child(classe)
    db.child("pages")
    pages={}
    try:
        pages = dict(db.get())
        page = db.bFind(pages,"idPage",int(data["idPage"][0]))
        if page != None:
            db.child(page)
        else:
            db.child("page"+str(len(pages)))
    except:
        db.child("page"+str(len(pages)))
    db.save({"idPage":int(data["idPage"][0]),"nom":data["nom"][0]})
    return json_response({"message":"Page '" + data["nom"][0] + "' ajouté à la classe '" + classes[classe]["nom"]+"'."})



@classes_api.route("/removePage",methods=['DELETE'])
@token_required_user
def removePage(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Classes")
    classes = dict(db.get())
    classe = db.bFind(classes,"idClasse",int(data["idClasse"][0]))
    db.child(classe)
    db.child("pages")
    pages = dict(db.get())
    page = db.bFind(pages,"idPage",int(data["idPage"][0]))
    pages.pop(page)
    pgs = {}
    for key, ex in pages.items():
        pgs["page"+str(len(pgs))] = ex
    #db.child(exercice)
    db.save(pgs)
    return json_response({"message":"Page '" + data["nom"][0] + "' enlevé de la classe '" + classes[classe]["nom"]+"'."})

