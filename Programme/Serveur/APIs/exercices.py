from logging import NullHandler
from flask import Flask, json, g, request
from flask_cors import CORS, cross_origin
from flask import Blueprint

from APIs.Token.token import *

exercices_api = Blueprint('exercices_api', __name__,url_prefix="/Exercices")
CORS(exercices_api)

def json_response(payload, status=200):
 return (json.dumps(payload), status, {'content-type': 'projects_apilication/json'})

@exercices_api.route("/getExercicesUser",methods=['GET'])
@token_required_user
def getExercices(check):
    db.reference("Server")
    db.child("Exercices")
    exos = {}
    try :
        exos = dict(db.get())
        exos = db.selectWithDateCondition(exos,"dateSuppression","dateCreation")
        exos = db.filtreBy(exos,"idUtilisateur",check["idUtilisateur"])
    except:
        pass
    finally:
        return json_response({"exercices" : exos})

@exercices_api.route("/getExercicesShared",methods=['GET'])
@token_required_user
def getExercicesShared(check):
    db.reference("Server")
    db.child("Exercices")
    exos = {}
    try :
        exos = dict(db.get())
        exos = db.selectWithDateCondition(exos,"dateSuppression","dateCreation")
        exos = db.filtreBy(exos,"shared",True)
    except:
        pass
    finally:
        return json_response({"exercices" : exos})

@exercices_api.route("/creerExercice",methods=['GET'])
@token_required_user
def creerExercice(check):
    data = request.args.to_dict(flat=False)  
    
    
    db.reference("Server/Exercices")
    lenElems = 0
    idExercice = int(uuid.uuid4())% 1000000
    try:
        lenElems = len(dict(db.get()))
        while db.bFind(db.get(),"idExercice",idExercice) != None:
            idExercice = int(uuid.uuid4())% 1000000
        db.child("exercice"+ str(lenElems))
    except:
        db.child("exercice"+ str(lenElems))
    new_Exercice = {"idExercice": idExercice, "nom":data['nom'][0], "idUtilisateur":check["idUtilisateur"], "shared":False, "dateCreation" : str(datetime.utcnow()) , "dateSuppression" : str(datetime.utcnow() - timedelta(seconds=1)),"difficulte" : data['difficulte'][0]}
    db.save(new_Exercice)
    return json_response({'message': 'enregistrement reussi'})

@exercices_api.route("/deleteExercice",methods=['DELETE'])
@token_required_user
def deleteExercice(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Exercices")
    exercices = dict(db.get())
    exercice = db.bFind(exercices,"idExercice",int(data["idExercice"][0]))
    db.child(exercice)
    db.update("dateSuppression",str(datetime.utcnow()))

    return json_response({"message" : "Exercice supprimée."})

@exercices_api.route("/modifDifficulteExercice",methods=['PUT'])
@token_required_user
def modifDifficulteExercice(check):
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Exercices")
    exercices = dict(db.get())
    exercice = db.bFind(exercices,"idExercice",int(data["idExercice"][0]))
    db.child(exercice)
    db.update("difficulte",data["difficulte"][0])

    return json_response({"message" : "Difficulté modifiée."})

@exercices_api.route("/loadExercice",methods=['GET'])
@token_required_eleve
def load(check):
    data = request.args.to_dict(flat=False)
    idExo = data['idExo'][0]
    db.reference("Server")
    exercices = db.sortBy("Exercices")
    for key, value in exercices.items():
        if(value["idExercice"] == idExo):
            return json_response(value)

    return json_response({})