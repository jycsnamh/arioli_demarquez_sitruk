from logging import NullHandler
from flask import Flask, json, g, request
from flask_cors import CORS, cross_origin
from flask import Blueprint

from APIs.Token.token import *

pages_api = Blueprint('pages_api', __name__,url_prefix="/Pages")
CORS(pages_api)

def json_response(payload, status=200):
 return (json.dumps(payload), status, {'content-type': 'projects_apilication/json'})

@pages_api.route("/creerPage",methods=['GET'])
@token_required_user
def creerPage(check):
    data = request.args.to_dict(flat=False)  
    
    
    db.reference("Server/Pages")
    lenElems = 0
    idPage = int(uuid.uuid4())% 1000000
    try:
        lenElems = len(dict(db.get()))
        while db.bFind(db.get(),"idPage",idPage) != None:
            idPage = int(uuid.uuid4())% 1000000
        db.child("Page"+ str(lenElems))
    except:
        db.child("Page"+ str(lenElems))
    new_Page = {"idPage": idPage, "nom":data['nom'][0],"idUtilisateur":check["idUtilisateur"],"dateCreation" : str(datetime.utcnow()) , "dateSuppression" : str(datetime.utcnow() - timedelta(seconds=1))}
    db.save(new_Page)
    return json_response({'message': 'enregistrement reussi'})




@pages_api.route("/getPagesUser",methods=['GET'])
@token_required_user
def getPages(check): 
    db.reference("Server")
    db.child("Pages")
    pages = {}
    try:
        pages = dict(db.get())
        pages = db.selectWithDateCondition(pages,"dateSuppression","dateCreation")
        pages = db.filtreBy(pages,"idUtilisateur",check["idUtilisateur"])
    except :
        pass
    finally :
        return json_response(pages)


@pages_api.route("/getPagesShared",methods=['GET'])
@token_required_user
def getPagesShared(check): 
    db.reference("Server")
    db.child("Pages")
    pages = {}
    try:
        pages = dict(db.get())
        pages = db.selectWithDateCondition(pages,"dateSuppression","dateCreation")
        pages = db.filtreBy(pages,"shared",True)
    except :
        pass
    finally :
        return json_response(pages)



@pages_api.route("/deletePage",methods=['DELETE'])
@token_required_user
def deletePage(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Pages")
    pages = dict(db.get())
    page = db.bFind(pages,"idPage",int(data["idPage"][0]))
    db.child(page)
    db.update("dateSuppression",str(datetime.utcnow()))

    return json_response({"message" : "Page supprimée."})


@pages_api.route("/addExercice",methods=['POST'])
@token_required_user
def addExercice(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Pages")
    pages = dict(db.get())
    page = db.bFind(pages,"idPage",int(data["idPage"][0]))
    db.child(page)
    db.child("exercices")
    exercices={}
    try:
        exercices = dict(db.get())
        exercice = db.bFind(pages,"idExercice",int(data["idExercice"][0]))
        if exercice != None:
            db.child(exercice)
        else:
            db.child("exercice"+str(len(exercices)))
    except:
        db.child("exercice"+str(len(exercices)))
    db.save({"idExercice":int(data["idExercice"][0]),"nom":data["nom"][0]})
    return json_response({"message":"Exercice '" + data["nom"][0] + "' ajouté à la page '" + pages[page]["nom"]+"'."})



@pages_api.route("/removeExercice",methods=['DELETE'])
@token_required_user
def removeExercice(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Pages")
    pages = dict(db.get())
    page = db.bFind(pages,"idPage",int(data["idPage"][0]))
    db.child(page)
    db.child("exercices")
    exercices = dict(db.get())
    exercice = db.bFind(exercices,"idExercice",int(data["idExercice"][0]))
    exercices.pop(exercice)
    exos = {}
    for key, ex in exercices.items():
        exos["exercice"+str(len(exos))] = ex
    #db.child(exercice)
    db.save(exos)
    return json_response({"message":"Exercice '" + data["nom"][0] + "' enlevé de la page '" + pages[page]["nom"]+"'."})


@pages_api.route("/getExercices",methods=['GET'])
@token_required_user
def getExercices(check): 
    data = request.args.to_dict(flat=False) 
    db.reference("Server")
    db.child("Pages")
    pages = dict(db.get())
    page = db.bFind(pages,"idPage",int(data["idPage"][0]))
    db.child(page)
    db.child("exercices")
    exercices = {}
    try:
        exercices = dict(db.get())
    except:
        pass
    finally:
        return json_response(exercices)