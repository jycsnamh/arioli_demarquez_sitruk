

from flask import Blueprint

#APIs
from APIs.classes import *
from APIs.editor import *
from APIs.eleves import *
from APIs.exercices import *
from APIs.pages import *
from APIs.utilisateurs import *

#exec
from firebase.cred import *


if __name__ == "__main__":
    
    App = AppFlask(__name__)
    
    #App.register(init_api, '/Init')
    App.register(classes_api, '/Classes')
    App.register(editor_api, '/Editor')
    App.register(eleves_api, '/Eleves')
    App.register(exercices_api, '/Exercices')
    App.register(pages_api, '/Pages')
    App.register(utilisateurs_api, '/Utilisateurs')

    App.start(5000,True)