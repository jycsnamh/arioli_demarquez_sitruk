import firebase_admin
from firebase_admin import credentials
from firebase_admin import storage

cred = credentials.Certificate("./firebase/projet-architecture-m1-21-22-firebase-adminsdk-df8h7-ff7431a116.json")
default_app = firebase_admin.initialize_app(cred, {
    'databaseURL':"https://projet-architecture-m1-21-22-default-rtdb.europe-west1.firebasedatabase.app/",
    'storageBucket': 'projet-architecture-m1-21-22.appspot.com'
    })

bucket = storage.bucket()