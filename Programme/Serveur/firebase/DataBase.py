from firebase_admin import db
from datetime import *

class DataBase :
    def __init__(self):
        self.ref = None

    def reference(self,dbs="/"):
        self.ref = db.reference(dbs)
    
    def save(self,content):
        self.ref.set(content)
    
    def update(self,name,value):
        self.ref.update({name : value})
    
    def child(self,child):
        self.ref = self.ref.child(child)

    def get(self):
        return self.ref.get()
    
    def selectWithDateCondition(self,get,LConditionPlusPetit,RConditionPlusGrand):
        for key,value in dict(get).items():
            if datetime.strptime(value[LConditionPlusPetit], '%Y-%m-%d %H:%M:%S.%f') > datetime.strptime(value[RConditionPlusGrand], '%Y-%m-%d %H:%M:%S.%f'):
                get.pop(key)
        return get
    
    def filtreBy(self,content,field,v):
        l = []
        for key,value in content.items():
            if value[field] != v :
                l.append(key)
        for s in l:
            content.pop(s)
        return content
    
    def searchAndUpdate(self,content,searchField,seachValue,updateField,updateValue):
        for key, value in content.items():
            if(value[searchField] == seachValue):
                self.ref.child(key).update({updateField:updateValue})
    
    def bFind(self,content,searchField,seachValue):
        for key, value in content.items():
            if searchField in value:
                if(value[searchField] == seachValue):
                    return key
        return None
        
    def sortBy(self,field):
        return self.ref.order_by_child(field).get()

    def getLast(self,field):
        return self.ref.order_by_child(field).limit_to_last(1).get()

    def getFirst(self,field):
        return self.ref.order_by_child(field).limit_to_first(1).get()

    def findAll(self,field,value):
        return self.ref.order_by_child(field).equal_to(value).get()

    def delete(self,content,searchField,data):
        for key, value in content.items():
            if(value[searchField] == data):
                self.ref.child(key).set({})

    def deleteAll(self,content):
        for key, value in content.items():
            self.ref.child(key).set({})