from tkinter import *
from utils.Interpreter import Interpreter
from editor.components.items.Son import Son
from editor.components.items.Texte import Texte
from editor.menus.MenuSon import MenuSon
from editor.menus.MenuTexte import MenuTexte
from editor.components.items.Img import Img
from editor.menus.MenuImage import MenuImage

from utils.DragManager import DragManager

class ToolBox:
    def __init__(self,root,sess,globs):
        self.children = []
        self.sess = sess
        self.globs = globs
        self.root = root
        self.toolbox = Frame(self.root)
        self.id = 0
        self.x = 0
        self.y = 0

    def getToolBox(self):
        return self.toolbox

    def getChildren(self):
        return self.children
    
    def add(self,child):
        self.children.append(child)

    def formatEdit(self,root):
        btn_edit_toolbox = Button(root,text="Configuration\nToolBox",width=10,height=3)
        btn_edit_toolbox.bind("<1>",self.panneauConfiguration)
        return btn_edit_toolbox

    def panneauConfiguration(self,event):
        self.panneau = Toplevel()
        labelTitre = Label(self.panneau,text="Panneau de configuration de la ToolBox")
        labelTitre.pack(side=TOP)
        self.frame = Frame(self.panneau)
        self.frame.pack(side=TOP)
        self.frameBtn = Frame(self.panneau)
        self.mainOptions()
        self.frameBtn.pack(side=TOP)
        btn_close = Button(self.panneau,text="Fermer")
        btn_close.pack(side=BOTTOM)
        btn_close.bind("<1>",lambda event, fen=self.panneau : self.Quitter(event,fen))
        self.panneau.mainloop()

    def showToolBox(self):
        interpreteur = Interpreter(self.globs)
        interpreteur.load(self.frame,self.sess,False,"ToolBox",TOP)

    def mainOptions(self,event=None):
        self.clean(self.frameBtn)
        self.clean(self.frame)
        self.showToolBox()
        f = Frame(self.frameBtn)
        f.pack(side=TOP)
        btn_addChild = Button(f,text="Ajouter un item dans la ToolBox",width=30,height=3,bg="yellow")
        btn_addChild.pack(side=LEFT)

        btn_removeChild = Button(f,text="Retirer un item de la ToolBox",width=30,height=3,bg="orange")
        btn_removeChild.pack(side=RIGHT)

        btn_manageChildren = Button(f,text="Management des items de la ToolBox",width=30,height=3,bg="red")
        btn_manageChildren.pack(side=RIGHT)

        btn_cleanToolBox = Button(f,text="Effacer tous les items de la ToolBox",width=30,height=3,bg="violet")
        btn_cleanToolBox.pack(side=RIGHT)

        btn_addChild.bind("<1>", self.addChild)
        btn_removeChild.bind("<1>", self.removeChild)
        btn_manageChildren.bind("<1>", self.manageChildren)
        btn_cleanToolBox.bind("<1>", self.cleanChildren)

    def cleanChildren(self,event):
        for child in self.getChildren():
            child.destroy()
        self.children.clear()
        self.sess.update(self)
        self.clean(self.frame)
        self.showToolBox()

    def manageChildren(self,event):
        self.clean(self.frame)
        btn_retour = Button(self.frameBtn,text="Retour",bg="grey",activeforeground="orange",height=4,width=30)
        btn_retour.pack(side=LEFT)
        self.showToolBox()
        for scf in self.frame.winfo_children():
            for c in scf.winfo_children():
                for r in c.winfo_children():
                    for child in r.winfo_children():
                        child.bind("<1>",self.selectItem)
        btn_retour.bind("<1>", self.backFromManage)

    def selectItem(self,event):
        x,y = event.widget.winfo_pointerxy()
        target = event.widget.winfo_containing(x,y)
        ind = self.find(target.id)
        self.clean(self.frameBtn)
        if target.type.find("Image") != -1:
            self.MenuImage(self.getChildren()[ind])
        elif target.type.find("Son") != -1:
            self.MenuSon(self.getChildren()[ind])
        elif target.type.find("Texte") != -1:
            self.MenuTexte(self.getChildren()[ind])

    def MenuImage(self,widget):
        menu = MenuImage(self.root,widget,None,None,self.sess,self.globs, None, self)
        btn_retour = Button(self.frameBtn,text="Retour",bg="grey",activeforeground="orange",height=4,width=30)
        btn_retour.pack(side=LEFT)
        btn_selectImg = Button(self.frameBtn ,text = "Selectionner dans la bibliothèque une image à afficher")# , command=self.modifierImage)
        btn_selectImg.pack(side=LEFT)
        btn_modifTaille = Button(self.frameBtn ,text = "Modifier la taille de l'image")#, command=self.modifierTailleImage)
        btn_modifTaille.pack(side=LEFT)
        #btn_split = Button(self.frameBtn ,text = "Séparer l'image en plusieurs morceaux")#, command=self.splitImage)
        #btn_split.pack(side=LEFT)
        btn_modifVal = Button(self.frameBtn ,text = "Modifier la valeur associée à l'image")#, command=self.modifierValue)
        btn_modifVal.pack(side=LEFT)
        
        btn_retour.bind("<1>", self.backFromManage)
        btn_selectImg.bind("<1>",menu.modifierImage)
        btn_modifTaille.bind("<1>",menu.modifierTailleImage)
        #btn_split.bind("<1>",menu.splitImage)
        btn_modifVal.bind("<1>",menu.modifierValue)

    def MenuSon(self,widget):
        menu = MenuSon(self.root,widget,None,None,self.sess,self.globs,None,self)
        btn_retour = Button(self.frameBtn,text="Retour",bg="grey",activeforeground="orange",height=4,width=30)
        btn_retour.pack(side=LEFT)
        btn_selectSon = Button(self.frameBtn ,text = "Selectionner un son dans la bibliothèque")
        btn_selectSon.pack(side=LEFT)
        btn_modifText = Button(self.frameBtn ,text = "Modifier le texte du bouton")
        btn_modifText.pack(side=LEFT)
        btn_modifTail = Button(self.frameBtn ,text = "Modifier la taille du bouton du son")
        btn_modifTail.pack(side=LEFT)
        btn_playStop = Button(self.frameBtn ,text = "Lire/Arreter le son")
        btn_playStop.pack(side=LEFT)
        btn_modifVal = Button(self.frameBtn ,text = "Modifier la valeur associée au son")
        btn_modifVal.pack(side=LEFT)

        btn_retour.bind("<1>", self.backFromManage)
        btn_selectSon.bind("<1>",menu.modifierSon)
        btn_modifText.bind("<1>",menu.modifierTexteSon)
        btn_modifTail.bind("<1>",menu.modifierTailleSon)
        btn_playStop.bind("<1>",menu.playSon)
        btn_modifVal.bind("<1>",menu.modifierValue)

    def MenuTexte(self,widget):
        menu = MenuTexte(self.root,widget,None,None,self.sess,self.globs,None,self)
        btn_retour = Button(self.frameBtn,text="Retour",bg="grey",activeforeground="orange",height=4,width=30)
        btn_retour.pack(side=LEFT)
        btn_modifText = Button(self.frameBtn ,text = "Modifier le texte")
        btn_modifText.pack(side=LEFT)
        btn_modifTail = Button(self.frameBtn ,text = "Modifier la taille de la zone de texte")
        btn_modifTail.pack(side=LEFT)
        btn_modifCoul = Button(self.frameBtn ,text = "Modifier la couleur du texte")
        btn_modifCoul.pack(side=LEFT)

        btn_retour.bind("<1>", self.backFromManage)
        btn_modifText.bind("<1>",menu.modifierTexte)
        btn_modifTail.bind("<1>",menu.modifierTailleZoneTexte)
        btn_modifCoul.bind("<1>",menu.modifierCouleurTexte)

    def backFromManage(self,event):
        self.mainOptions()

    def removeChild(self,event):
        self.clean(self.frame)
        self.showToolBox()
        for scf in self.frame.winfo_children():
            for c in scf.winfo_children():
                for r in c.winfo_children():
                    for child in r.winfo_children():
                        child.bind("<1>",self.deleteChild)

    def deleteChild(self,event):
        x,y = event.widget.winfo_pointerxy()
        target = event.widget.winfo_containing(x,y)
        ind = self.find(target.id)
        self.getChildren()[ind].destroy()
        self.getChildren().pop(ind)
        self.sess.update(self)
        target.destroy()
        self.clean(self.frame)
        self.showToolBox()
        self.backFromRemove()


    def backFromRemove(self):
        for scf in self.frame.winfo_children():
            for c in scf.winfo_children():
                for s in c.winfo_children():
                    for child in s.winfo_children():
                        try:
                            child.unbind("<1>")
                        except: 
                            pass
        self.mainOptions()


    def find(self,id):
        for i in range(len(self.getChildren())):
            if int(self.getChildren()[i].id) == int(id):
                return i
        return -1

    def addChild(self,even,):
        self.clean(self.frameBtn)
        self.clean(self.frame)
        btn_retour = Button(self.frameBtn,text="Retour",bg="grey",activeforeground="orange",height=4,width=30)
        btn_retour.pack(anchor=NW)
        self.showToolBox()
        f = Frame(self.frameBtn)
        f.pack(side=TOP)
        texte = Button(f,text="Texte",bg="#cccccc",activeforeground="orange",height=4,width=30)
        texte.pack(side=LEFT)

        image = Button(f,text="Image",bg="#cccccc",activeforeground="orange",height=4,width=30)
        image.pack(side=RIGHT)

        
        son = Button(f,text="Son",bg="#cccccc",activeforeground="orange",height=4,width=30)
        son.pack(side=RIGHT)

        #forme = Button(f,text="Forme",bg="#cccccc",activeforeground="orange",height=4,width=30)
        #forme.pack(side=RIGHT)

        btn_retour.bind("<1>", self.mainOptions)
        texte.bind("<1>", self.addTexte)
        image.bind("<1>", self.addImage)
        son.bind("<1>", self.addSon)

    

    def addSon(self,event):
        son = Son("Son",self.toolbox,MenuSon,text="Nouveau son")
        son.pack()
        son.id = len(self.root.winfo_children()) + len(self.children)
        son.dm = DragManager(self.toolbox,son,self.sess,None,self.globs,True,False,False,True,True)
        son.dm.start()
        self.children.append(son)
        self.sess.update(self)
        self.clean(self.frame)
        self.showToolBox()

    def addTexte(self,event):
        texte = Texte("Texte",self.toolbox,MenuTexte)
        texte.insert(1.0, "Nouveau texte.")
        texte.configure(state='disabled')
        texte.configure(background="white")
        texte.configure(relief=FLAT)
        texte.pack()
        texte.id = len(self.root.winfo_children()) + len(self.children)
        texte.tag = []
        texte.dm = DragManager(self.toolbox,texte,self.sess,None,self.globs,True,False,False,True,True)
        texte.dm.start()
        self.children.append(texte)
        self.sess.update(self)
        self.clean(self.frame)
        self.showToolBox()

    def addImage(self,event):
        
        image = Img("Image",self.toolbox,MenuImage,bg="black",width=10,height=10)
        image.pack()
        image.id = len(self.root.winfo_children()) + len(self.children)
        image.dm = DragManager(self.toolbox,image,self.sess,None,self.globs,True,False,False,True,True)
        image.dm.start()
        self.children.append(image)
        self.sess.update(self)
        self.clean(self.frame)
        self.showToolBox()





    def getChild(self,event):
        x,y = event.widget.winfo_pointerxy()
        child = event.widget.winfo_containing(x,y)

    
    def clean(self,frame):
        for child in frame.winfo_children():
            child.destroy()

    def Quitter(self,event,fen):
        self.sess.update(self)
        fen.quit()
        fen.destroy()