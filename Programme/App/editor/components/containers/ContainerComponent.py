from tkinter import *
from editor.menus.MenuTexte import *
from editor.menus.MenuSon import *
from editor.menus.MenuImage import *

from editor.components.items.Son import *
from editor.components.items.Img import *
from editor.components.items.Texte import *


class ContainerComponent(Frame):
    def __init__(self,Type,root,menu,sess, *args, **kwargs):
        super().__init__(root, *args, **kwargs)
        self.child = None
        self.type = Type
        self.sess = sess
        self.widget = self
        self.menu = menu
        print(self.menu)
        self.id = 0
        self.resultatAttendu = ""
        self.x = 0
        self.y = 0
        self.dm = None

    def getMenu(self):
        return self.menu

    def getChild(self):
        return self.child

    def getResultatAttendu(self):
        return self.resultatAttendu

    def setResultatAttendu(self,res):
        self.resultatAttendu = res

    def changeToContainerImage(self,DragManager,globs,edit = True):
        if self.child != None:
            self.child.destroy()
        self.child = Img("Label",self,MenuImage)
        self.child.pack()
        self.child.config(width=40)
        self.child.config(height=10)
        self.child.config(bg="grey")
        
        self.child.dm = DragManager(self,self.child,self.sess,self,globs,edit,True,True,edit)
        self.child.dm.start()

    def changeToContainerText(self,DragManager,globs,edit = True):
        if self.child != None:
            self.child.destroy()
        self.child = Texte("Text",self,MenuTexte)
        self.child.pack()
        self.child.config(width=40)
        self.child.config(height=10)
        self.child.config(state=DISABLED)
        self.child.config(bg="grey")
        
        self.child.dm = DragManager(self,self.child,self.sess,self,globs,edit,True,True,edit)
        self.child.dm.start()

    def changeToContainerSon(self,DragManager,globs,edit = True):
        if self.child != None:
            self.child.destroy()
        self.child = Son("Button",self,MenuSon)
        self.child.pack()
        self.child.config(width=40)
        self.child.config(height=10)
        self.child.config(bg="grey")
        self.child.dm = DragManager(self,self.child,self.sess,self,globs,edit,True,True,edit)
        self.child.dm.start()


    def reinitContainer(self,DragManager,globs):
        if str(self.child.__class__).find("Image") != -1 :
            self.changeToContainerImage(DragManager,globs,False)
        elif str(self.child.__class__).find("Texte") != -1 :
            self.changeToContainerText(DragManager,globs,False)
        elif str(self.child.__class__).find("Son") != -1 :
            self.changeToContainerSon(DragManager,globs,False)


    def clone(self,parent):
        #parent = self.widget.nametowidget(self.widget.winfo_parent())
        cls = self.widget.__class__
        clone = cls(self.type,parent,self.menu,self.sess)
        for key in self.widget.configure():
            if key != 'master' and key != "class" and key != "colormap" and key != "visual" and key != "container":
                clone.configure({key: self.widget.cget(key)})
        return clone