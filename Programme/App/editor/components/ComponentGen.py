from tkinter import *

class ComponentGen(Frame,Text,Label,Button):
    def __init__(self,Type,parent,widget,sess,menu,*args, **kwargs):
        if Type == "Button":
            super(Button).__get__(widget)
        elif Type == "Label":
            super(Label).__get__(widget)
        elif Type == "Frame":
            super(Frame).__get__(widget)
        elif Type == "Text":
            super(Text).__get__(widget)
        self.type = Type
        self.id = 0
        self.sess = sess
        self.widget = widget
        self.menu = menu
        self.x = 0
        self.y = 0
        self.dm = None

    def clone(self,parent):
        #parent = self.widget.nametowidget(self.widget.winfo_parent())
        cls = self.widget.__class__
        clone = None
        if str(cls).find("Container") != -1:
            clone = cls(self.type,parent,self.menu,self.sess)
        else:
            clone = cls(self.type,parent,self.menu)
        for key in self.widget.configure():
            if key != 'master':
                clone.configure({key: self.widget.cget(key)})
        return clone