from tkinter import *

class Bouton(Button):
    def __init__(self, Type,container,menu, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        self.actions = ""
        self.value = ""
        self.id = 0
        self.type = Type
        self.widget = self
        self.menu = menu
        self.x = 0
        self.y = 0
        self.dm = None

    def getMenu(self):
        return self.menu
        
    def getActions(self):
        return self.actions

    def setActions(self,act):
        self.actions = act

    def getValue(self):
        return self.value

    def setValue(self,val):
        self.value = val

    def clone(self,parent):
        #parent = self.widget.nametowidget(self.widget.winfo_parent())
        cls = self.widget.__class__    
        clone = cls(self.type,parent,self.menu)
        for key in self.widget.configure():
            if key != 'master':
                clone.configure({key: self.widget.cget(key)})
        return clone