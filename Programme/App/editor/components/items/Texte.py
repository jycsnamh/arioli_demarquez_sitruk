from tkinter import *


class Texte(Text):
    def __init__(self,Type, container,menu, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        self.type = Type
        self.widget = self
        self.menu = menu
        self.id = 0
        self.tag = []
        self.active = "DISABLED"
        self.resultatAttendu = ""
        self.x = 0
        self.y = 0
        self.dm = None
    
    def getMenu(self):
        return self.menu

    def getResultatAttendu(self):
        return self.resultatAttendu

    def setResultatAttendu(self,res):
        self.resultatAttendu = res
    
    def set(self,str):
        self.widget.config(state=NORMAL)
        self.widget.delete(1.0,END)
        self.widget.insert("1.0",str)
        self.widget.config(state=DISABLED)

    def clone(self,parent):
        #parent = self.widget.nametowidget(self.widget.winfo_parent())
        cls = self.__class__
        c = cls(self.type,parent,self.menu)
        for key in self.configure():
            if key != 'master':
                c.configure({key: self.cget(key)})
        c.config(state=NORMAL)
        c.insert("1.0",self.get(1.0,END))
        c.config(state=DISABLED)
        return c