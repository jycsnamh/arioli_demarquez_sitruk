from tkinter import *
from tkinter import ttk
import tkinter.font as font


class Forme():
    def __init__(self,Type,container,menu, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        self.container = Canvas(container)
        self.value = ""
        self.id = 0
        self.type = Type
        self.widget = self
        self.menu = menu
        self.x = 0
        self.y = 0
        self.dm = None
        
    def getMenu(self):
        return self.menu
        
    def drawRectangle(self):
        rect = self.container.create_rectangle(10,10,50,50)

    
    def clone(self,parent):
        #parent = self.widget.nametowidget(self.widget.winfo_parent())
        cls = self.widget.__class__
        clone = cls(self.type,parent,self.menu)
        for key in self.widget.configure():
            if key != 'master':
                clone.configure({key: self.widget.cget(key)})
        return clone