from tkinter import *

from utils.Sound import Sound

class Son(Button):
    def __init__(self, Type,container,menu, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        self.sound = Sound(self)
        self.fichier = ""
        self.value = ""
        self.id = 0
        self.type = Type
        self.widget = self
        self.menu = menu
        self.x = 0
        self.y = 0
        self.dm = None
    
    def getMenu(self):
        return self.menu
        
    def init(self,son):
        self.fichier = son
        self.sound.load(son)

    def setTexte(self,texte):
        self.config(text=texte)
        
    def getSound(self):
        return self.sound

    def getValue(self):
        return self.value

    def setValue(self,val):
        self.value = val

    def clone(self,parent):
        #parent = self.widget.nametowidget(self.widget.winfo_parent())
        cls = self.widget.__class__
        clone = cls(self.type,parent,self.menu)
        for key in self.widget.configure():
            if key != 'master':
                clone.configure({key: self.widget.cget(key)})
        return clone