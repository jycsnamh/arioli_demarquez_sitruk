from logging import info
from tkinter import *
from tkinter import ttk
from tkinter import font
from PIL import ImageTk
import requests
from requests.sessions import session
from editor.components.containers.ToolBox import ToolBox
from editor.menus.MenuForme import MenuForme
from editor.menus.MenuToolBox import MenuToolBox
from editor.menus.MenuZoneDessin import MenuZoneDessin
from editor.menus.MenuBouton import MenuBouton
from editor.menus.MenuContainer import MenuContainer
from editor.menus.MenuImage import MenuImage
from editor.menus.MenuSon import MenuSon
from editor.menus.MenuTexte import MenuTexte
from editor.components.items.Bouton import Bouton
from utils.InfoBulle import *
from utils.Sess import *
from utils.Globs import *
from utils.Interpreter import *
from editor.components.items.Img import *

from editor.components.items.Texte import *
from utils.DragManager import *


class Editor():

    listeDrop = []
    bLibs = False
    bContainers = False
    bItems = False
    def __init__(self,root,token,parent,id,nom,load = False):
        self.globs = Globs()
        self.globs.createGlobs("listeDrop", Editor.listeDrop)
        
        self.interpreter = Interpreter(self.globs)
        self.root = root
        self.token = token
        self.parent = parent
        self.id = id
        self.nom = nom
        self.sess = Sess(self.root,self.token,self.id,self.nom) # variable qui va contenir le script de l'exercice

        
    
    
        for child in self.root.winfo_children():
            child.destroy()
        

        self.tools = Frame(self.root)
        self.tools.pack(side=RIGHT)

        self.render = Frame(self.root,bg = "#dddddd",width=(self.root.winfo_width() - self.tools.winfo_width()),height=self.root.winfo_height()-27)
        self.render.pack_propagate(0)
        self.render.pack(anchor=NW)
        
        
        # initialisation des variables globales sur les types de menu
        menuBouton = MenuBouton
        self.globs.createGlobs(str(menuBouton), menuBouton)
        menuContainer = MenuContainer
        self.globs.createGlobs(str(menuContainer), menuContainer)
        menuForme = MenuForme
        self.globs.createGlobs(str(menuForme), menuForme)
        menuImage = MenuImage
        self.globs.createGlobs(str(menuImage), menuImage)
        menuSon = MenuSon
        self.globs.createGlobs(str(menuSon), menuSon)
        menuTexte = MenuTexte
        self.globs.createGlobs(str(menuTexte), menuTexte)
        menuZoneDessin = MenuZoneDessin
        self.globs.createGlobs(str(menuZoneDessin), menuZoneDessin)
        

        # initialisation des variables globales sur les types des composants
        self.genBouton = Bouton("Bouton",self.render,menu=self.globs.getValue(str(menuBouton)), text="Nouveau bouton",relief=RAISED)
        self.globs.createGlobs(str(self.genBouton.__class__), self.genBouton.__class__)
        self.genImage = Img("Image",self.render,menu=self.globs.getValue(str(menuImage)), bg="black",width=30,height=10)
        self.globs.createGlobs(str(self.genImage.__class__), self.genImage.__class__)
        self.genTexte = Texte("Texte",self.render,menu=self.globs.getValue(str(menuTexte)))
        self.globs.createGlobs(str(self.genTexte.__class__), self.genTexte.__class__)
        self.genContainer = ContainerComponent("Container",self.render,menu=self.globs.getValue(str(menuContainer)), bg="black",width=280,height=150,sess=self.sess)
        self.globs.createGlobs(str(self.genContainer.__class__), self.genContainer.__class__)
        self.genSon = Son("Son",self.render,menu=self.globs.getValue(str(menuSon)),text="Nouveau son")
        self.globs.createGlobs(str(self.genSon.__class__), self.genSon.__class__)
        self.genToolBox = ToolBox(self.render,sess=self.sess,globs=self.globs)
        self.globs.createGlobs(str(self.genToolBox.__class__), self.genToolBox.__class__)

        
        
        self.status = Frame(self.root,bg="#cccccc", width=(self.root.winfo_width() - self.tools.winfo_width()),height=27)
        self.status.pack_propagate(0)
        self.status.pack(side=BOTTOM)
        label_info = Label(self.status,bg="#cccccc",text="")
        label_info.pack()
        self.info = InfoBulle(self.status,label_info)

        

        self.param = Frame(self.tools)
        self.param.pack(side=TOP)

        self.composants = Frame(self.tools)
        self.composants.pack(side=TOP)

        btn_retour = Button(self.param,text="Retour",font=("Arial",13))
        btn_retour.pack(side=LEFT)
        btn_help = Button(self.param,text="?",bg="red",font=("Arial",13))
        btn_help.pack(side=LEFT)
        btn_save = Button(self.param,text="Sauvegarder",font=("Arial",13))
        btn_save.pack(side=RIGHT)

        btn_Containers = Button(self.composants,text="Containers",bg="#555555",highlightcolor="orange",height=4,width=30)
        btn_Containers.pack(side=TOP)
        self.root.update()

        self.containers = Frame(self.composants,height=30*6,width=self.tools.winfo_width())
        self.containers.pack_propagate(0)
        self.containers.pack(side=TOP)

        btn_Items = Button(self.composants,text="Items",bg="#555555",highlightcolor="orange",height=4,width=30)
        btn_Items.pack(side=TOP)
        self.items = Frame(self.composants,height=30*10,width=self.tools.winfo_width())
        self.items.pack_propagate(0)
        self.items.pack(side=TOP)

        btn_render = Button(self.tools,text="Rendu", bg="cyan", width=30,height=4)
        btn_render.pack(side=BOTTOM)
        

        # zoneDeDessin = Button(self.containers,text="Zone de dessin",bg="grey",activeforeground="orange",height=4,width=30)
        # zoneDeDessin.pack_propagate(0)
        # zoneDeDessin.pack(side=TOP)
        
        container = Button(self.containers,text="Container",bg="grey",activeforeground="orange",height=4,width=30)
        container.pack_propagate(0)
        # container.pack(side=TOP)

        toolBox = Button(self.containers,text="Boite à outils",bg="grey",activeforeground="orange",height=4,width=30)
        toolBox.pack_propagate(0)
        # container.pack(side=TOP)

        bouton = Button(self.items,text="Bouton",bg="grey",activeforeground="orange",height=4,width=30)
        bouton.pack_propagate(0)
        # bouton.pack(side=TOP)

        texte = Button(self.items,text="Texte",bg="grey",activeforeground="orange",height=4,width=30)
        texte.pack_propagate(0)
        # texte.pack(side=TOP)

        image = Button(self.items,text="Image",bg="grey",activeforeground="orange",height=4,width=30)
        image.pack_propagate(0)
        # image.pack(side=TOP)

        
        son = Button(self.items,text="Son",bg="grey",activeforeground="orange",height=4,width=30)
        son.pack_propagate(0)
        # son.pack(side=TOP)

        # forme = Button(self.items,text="Forme",bg="grey",activeforeground="orange",height=4,width=30)
        # forme.pack_propagate(0)
        # son.pack(side=TOP)

        btn_Containers.bind("<1>",self.showHideContainers)
        btn_Items.bind("<1>",self.showHideItems)

        self.recBindButton(self.composants)
        
        bouton.bind("<1>",lambda event, sess=self.sess : self.addBouton(event,sess))
        texte.bind("<1>",lambda event, sess=self.sess : self.addTexte(event,sess))
        image.bind("<1>",lambda event, sess=self.sess : self.addImage(event,sess))
        son.bind("<1>",lambda event, sess=self.sess : self.addSon(event,sess))
        container.bind("<1>",lambda event, sess=self.sess : self.addContainer(event,sess))
        toolBox.bind("<1>",lambda event, sess=self.sess : self.addToolBox(event,sess))
    
        # chargement de l'exercice à modifier
        if load :
            self.sess.load(id)
            if self.sess.findIn("script"):
                self.interpreter.load(self.render,self.sess,True)
            else :
                self.sess.initScript()
        else :
            self.sess.initialisesession()

        btn_retour.bind("<1>", self.parent.MainPage)
        btn_help.bind("<1>",self.aide)
        btn_save.bind("<1>",lambda event, info=self.info : self.sess.save(event,info))
        btn_render.bind("<1>",self.visualisation)
        
    def aide(self,event):
        popup = Toplevel()
        frame = Frame(popup)
        frame.pack(side=TOP)
        t = Text(frame)
        t.insert("1.0","                               Tips\n")
        t.insert("2.0","\n           Pour configurer les composants,\n           n'oubliez pas de faire click droit sur eux !")
        t.pack(side=TOP)
        t.config(state=DISABLED)
        btnQuit = Button(popup,text="Fermer")
        btnQuit.pack(side=TOP)
        btnQuit.bind("<1>",lambda event, fen=popup: self.Quitter(event,fen))
        popup.mainloop()

    def recBindButton(self,widget):
        for child in list(widget.winfo_children()):
            if len(list(child.winfo_children())) != 0:
                self.recBindButton(child)
            else:
                child.bind("<Enter>", self.on_enter)
                child.bind("<Leave>", self.on_leave)

    def on_enter(self,event):
        event.widget['foreground'] = 'orange'

    def on_leave(self,event):
        event.widget['foreground'] = 'black'

    def showHideItems(self,event):
        if not Editor.bItems :
            for child in self.items.winfo_children():
                child.pack(side=TOP)
            Editor.bItems = True
        else:
            for child in self.items.winfo_children():
                child.pack_forget()
            Editor.bItems = False

    def showHideContainers(self,event):
        if not Editor.bContainers :
            for child in self.containers.winfo_children():
                child.pack(side=TOP)
            Editor.bContainers = True
        else:
            for child in self.containers.winfo_children():
                child.pack_forget()
            Editor.bContainers = False

    def addBouton(self,event,sess):
        bouton = self.genBouton.clone(self.render)
        bouton.pack()
        bouton.id = len(self.render.winfo_children())
        self.sess.formate(bouton)
        bouton.dm = DragManager(self.render,bouton,sess,None,self.globs,True,False)
        bouton.dm.start()

    def addSon(self,event,sess):
        son = self.genSon.clone(self.render)
        son.pack()
        son.id = len(self.render.winfo_children())
        self.sess.formate(son)
        son.dm = DragManager(self.render,son,sess,None,self.globs,True,False)
        son.dm.start()

    def addTexte(self,event,sess):
        texte = self.genTexte.clone(self.render)
        texte.set("Nouveau texte.")
        texte.configure(background="white")
        texte.configure(relief=FLAT)
        texte.pack()
        texte.id = len(self.render.winfo_children())
        texte.tag = []
        self.sess.formate(texte)
        texte.dm = DragManager(self.render,texte,sess,None,self.globs,True,False)
        texte.dm.start()

    def addImage(self,event,sess):
        
        image = self.genImage.clone(self.render)
        image.pack()
        image.id = len(self.render.winfo_children())
        self.sess.formate(image)
        image.dm = DragManager(self.render,image,sess,None,self.globs,True,False)
        image.dm.start()

    def addContainer(self,event,sess):
        container = self.genContainer.clone(self.render)
        container.pack()
        container.id = len(self.render.winfo_children())
        self.sess.formate(container)
        container.dm = DragManager(self.render,container,sess,None,self.globs,True,False)
        container.dm.start()


    def addToolBox(self,event,sess):
        toolbox = self.genToolBox
        toolbox.formatEdit(self.render).pack(anchor=NE)
        toolbox.id = len(self.render.winfo_children())
        self.sess.formate(toolbox)


    # 
    def visualisation(self,event):
        
        fenetre = Toplevel()
        fenetre.state('zoomed')
        # fenetre.config(bg = "white")
        self.interpreter.load(fenetre,self.sess,False,"Exec")
        btn_quit = Button(fenetre,text="Fermer")
        btn_quit.pack(side=BOTTOM)

        btn_quit.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        fenetre.mainloop()


    
    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()