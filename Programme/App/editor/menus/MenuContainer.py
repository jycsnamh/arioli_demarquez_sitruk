from tkinter import *
from editor.menus.MenuGen import MenuGen
from editor.components.containers.ContainerComponent import *


class MenuContainer(MenuGen):
    def __init__(self,root=None,widget=None,event=None,menu=None,sess=None,globs=None,DragManager=None,container = None):
        super().__init__(root,widget,event,menu,sess,globs,DragManager,container)
        
    

    # Container
    def Menu(self):
        self.menuType = Menu(self.menu)
        self.menuModifier = Menu(self.menu)
        self.menu.add_cascade(label='Type', menu=self.menuType)
        self.menu.add_cascade(label="Modifier",menu=self.menuModifier)
        self.menu.add_separator()
        self.menu.add_command (label = "Supprimer", command=self.on_delete)

        self.menuType.add_command(label="Container image", command=lambda DM= self.DragManager, globs=self.globs : self.widget.changeToContainerImage(DM,globs))
        self.menuType.add_command(label="Container texte", command=lambda DM= self.DragManager, globs=self.globs : self.widget.changeToContainerText(DM,globs))
        self.menuType.add_command(label="Container son", command=lambda DM= self.DragManager, globs=self.globs : self.widget.changeToContainerSon(DM,globs))
        
        #self.menuModifier.add_command (label = "La taille du container", command=self.modifierTailleContainer)
        #self.menuModifier.add_command (label = "Le nombre d'items pouvant être mis dans le container'", command=self.modifierNombreItemsDansContainer)
        self.menuModifier.add_command (label ="Le résultat attendu", command=self.modifierResultatAttendu)
