from tkinter import *
import tkinter.font as font
from editor.menus.MenuGen import MenuGen


class MenuBouton(MenuGen):
    def __init__(self,root,widget,event,menu,sess,globs,DragManager=None,container = None):
        super().__init__(root,widget,event,menu,sess,globs,DragManager,container)
        
    # Bouton    
    def Menu(self):
        self.menuModifier = Menu(self.menu)
        self.menu.add_command(label='Actions', command=self.modifierAction)
        self.menu.add_cascade(label="Modifier",menu=self.menuModifier)
        self.menu.add_separator()
        self.menu.add_command (label = "Supprimer", command=self.on_delete)
        
        self.menuModifier.add_command (label = "Le texte du bouton" , command=self.modifierTexteBouton)
        self.menuModifier.add_command (label = "La taille du bouton", command=self.modifierTailleBouton)
        self.menuModifier.add_command (label = "La couleur du bouton", command=self.modifierCouleurBouton)
        self.menuModifier.add_command (label = "La couleur du texte du bouton", command=self.modifierCouleurTexteBouton)

    # Section Modifier
    def modifierCouleurTexteBouton(self):

        listeCouleur=["white","yellow","orange","red","violet","blue","cyan","black"]
        fenetre = Toplevel()
        
        txt_X = Label(fenetre,text="Couleur : ")
        txt_X.pack()
        sliderCouleur = Scale(fenetre, from_=0 , to=len(listeCouleur)-1 ,orient=HORIZONTAL)
        sliderCouleur.pack()
        
        btn_valider = Button(fenetre,text="Terminer")
        btn_valider.pack()

        sliderCouleur.bind("<B1-Motion>",lambda event, list=listeCouleur : self.changeCouleurTexteBouton(event,list))
        btn_valider.bind("<1>", lambda event,fen=fenetre : self.Quitter(event,fen))

        fenetre.mainloop()

    def changeCouleurTexteBouton(self,event,list):
        self.widget.config(fg=list[event.widget.get()])

    def modifierCouleurBouton(self):

        listeCouleur=["white","yellow","orange","red","violet","blue","cyan","black"]
        fenetre = Toplevel()
        
        txt_X = Label(fenetre,text="Couleur : ")
        txt_X.pack()
        sliderCouleur = Scale(fenetre, from_=0 , to=len(listeCouleur)-1 ,orient=HORIZONTAL)
        sliderCouleur.pack()
        
        btn_valider = Button(fenetre,text="Terminer")
        btn_valider.pack()

        sliderCouleur.bind("<B1-Motion>",lambda event, list=listeCouleur : self.changeCouleurBouton(event,list))
        btn_valider.bind("<1>", lambda event,fen=fenetre : self.Quitter(event,fen))
        
        fenetre.mainloop()

    def changeCouleurBouton(self,event,list):
        self.widget.config(bg=list[event.widget.get()])


    def modifierTailleBouton(self):
        
        fenetre = Toplevel()
        
        txt_X = Label(fenetre,text="Longueur : ")
        txt_X.pack()
        sliderX = Scale(fenetre, from_=0 , to=100 ,orient=HORIZONTAL)
        sliderX.pack()
        txt_Y = Label(fenetre,text="Hauteur : ")
        txt_Y.pack()
        sliderY = Scale(fenetre, from_=0, to=50)
        sliderY.pack()
        txt_taille = Label(fenetre,text="Taille du texte du bouton : ")
        txt_taille.pack()
        sliderTailleTexte = Scale(fenetre,from_=1, to=50,orient=HORIZONTAL)
        sliderTailleTexte.pack()    

        btn_valider = Button(fenetre,text="Terminer")
        btn_valider.pack()

        sliderX.bind("<B1-Motion>",self.changeTailleXBouton)
        sliderY.bind("<B1-Motion>",self.changeTailleYBouton)
        sliderTailleTexte.bind("<B1-Motion>", self.changeTailleTexteBouton)
        
        btn_valider.bind("<1>", lambda event,fen=fenetre : self.Quitter(event,fen))
        
        fenetre.mainloop()

    def changeTailleYBouton(self,event):
        self.widget.config(height=event.widget.get())
        self.widget.pack_propagate(0)

    def changeTailleXBouton(self,event):
        self.widget.config(width=event.widget.get())
        self.widget.pack_propagate(0)

    def changeTailleTexteBouton(self,event):
        self.widget.config(font=font.Font(size=event.widget.get()))

    def modifierTexteBouton(self):
        fenetre = Toplevel()
        info = Label(fenetre,text="Appuyez sur 'enter' pour valider.",pady=10)
        info.pack()
        value = StringVar(fenetre)
        value.set("")
        entree = Entry(fenetre, textvariable=value, width=50)
        entree.pack()
        entree.bind("<Return>", lambda event, fen=fenetre : self.changeTextBouton(event,fen))
        
        fenetre.mainloop()

    def changeTextBouton(self,event,fen):
        self.widget.config(text=event.widget.get())
        self.Quitter(event,fen)
        
    