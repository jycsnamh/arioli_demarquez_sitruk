from tkinter import *
import PIL.Image, PIL.ImageTk
from utils.Bibliotheque import Bibliotheque
from editor.components.items.Img import *
from editor.menus.MenuGen import MenuGen

class MenuImage(MenuGen):
    def __init__(self,root=None,widget=None,event=None,menu=None,sess=None,globs=None,DragManager=None,container = None):
        super().__init__(root,widget,event,menu,sess,globs,DragManager,container)
        


    # Image
    def Menu(self):
        
        self.menu.add_command (label = "Selectionner dans la bibliothèque une image à afficher" , command=self.modifierImage)
        self.menu.add_command (label = "Modifier la taille de l'image", command=self.modifierTailleImage)
        self.menu.add_command (label = "Séparer l'image en plusieurs morceaux", command=self.splitImage)
        self.menu.add_command (label = "Modifier la valeur associée à l'image", command=self.modifierValue)
        self.menu.add_separator()
        self.menu.add_command (label = "Supprimer", command=self.on_delete)

    def modifierImage(self,event = None):
        libImage = Bibliotheque("Image",self.widget,self.sess,self.container)
        

    def modifierTailleImage(self,event = None):
        fenetre = Toplevel()
        self.width = self.widget.winfo_width()
        self.height = self.widget.winfo_height()
        img_X = Label(fenetre,text="Longueur : ")
        img_X.pack()
        sliderXImg = Scale(fenetre,from_=1, to=1200 ,orient=HORIZONTAL)
        sliderXImg.pack()
        
        txt_Y = Label(fenetre,text="Hauteur : ")
        txt_Y.pack()
        sliderYImg = Scale(fenetre,from_=1, to=800)
        sliderYImg.pack()

        #txt_XY = Label(fenetre,text="Proportionnel : ")
        #txt_XY.pack()
        #sliderXYImg = Scale(fenetre,from_=-self.root.winfo_height(), to=self.root.winfo_height(),orient=HORIZONTAL)
        #sliderXYImg.pack()

        btn_quit = Button(fenetre,text="Terminer")
        btn_quit.pack()

        sliderXImg.bind("<B1-Motion>", self.changeXImage)
        sliderYImg.bind("<B1-Motion>", self.changeYImage)
        #sliderXYImg.bind("<B1-Motion>",self.changeXYImage)

        btn_quit.bind("<1>",lambda event,fen=fenetre: self.Quitter(event,fen))

        fenetre.mainloop()

    def MAJTailleImage(self,event):
        self.changeXImage(event)
        self.changeYImage(event)

    def changeXImage(self,event):
        self.width = event.widget.get()
        self.widget.place(x=self.widget.winfo_rootx(),y=self.widget.winfo_rooty()-23,width=event.widget.get(),height=self.widget.winfo_height())
        img = PIL.Image.open(self.widget["text"])
        resized_image= img.resize((self.width,self.height),PIL.Image.ANTIALIAS)
        pho = PIL.ImageTk.PhotoImage(resized_image)
        self.widget.config(image=pho)
        self.widget.image = pho

    def changeYImage(self,event):
        self.height = event.widget.get()
        self.widget.place(x=self.widget.winfo_rootx(),y=self.widget.winfo_rooty()-23,width=self.widget.winfo_width(),height=event.widget.get())
        img = PIL.Image.open(self.widget["text"])
        resized_image= img.resize((self.width,self.height),PIL.Image.ANTIALIAS)
        pho = PIL.ImageTk.PhotoImage(resized_image)
        self.widget.config(image=pho)
        self.widget.image = pho

    def changeXYImage(self,event):
        self.height = self.height + event.widget.get() 
        self.width = self.width + event.widget.get()
        self.widget.place(x=self.widget.winfo_rootx(),y=self.widget.winfo_rooty()-23,width=self.width,height=self.height)
        img = PIL.Image.open(self.widget["text"])
        resized_image= img.resize((self.width,self.height),PIL.Image.ANTIALIAS)
        pho = PIL.ImageTk.PhotoImage(resized_image)
        self.widget.config(image=pho)
        self.widget.image = pho

    def splitImage(self):
        pass

   