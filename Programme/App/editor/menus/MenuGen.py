from tkinter import *
from tkinter import ttk

from utils.DragManager import DragManager

class MenuGen:
    def __init__(self,root,widget,event,menu,sess,globs,DragManager,container):
        self.setParams(root,widget,event,menu,sess,globs,DragManager,container)

    def setParams(self,root,widget,event,menu,sess,globs,DragManager,container):
        self.root = root
        self.widget = widget
        self.menu = menu
        self.sess = sess
        self.globs = globs
        self.DragManager = DragManager
        self.container = container

    def modifierValue(self,event = None):
        fenetre = Toplevel()


        frame = Frame(fenetre)
        frame.pack(side=TOP)
        labelInfo = Label(frame,text="Saisir la valeur du composant :")
        labelInfo.pack(side=TOP)
        val = Text(frame)
        val.pack(side=TOP)

        btnQuit = Button(fenetre,text="Valider")
        btnQuit.pack(side=TOP)
        btnQuit.bind("<1>",lambda event, fen=fenetre, val=val : self.ValiderValue(event,fen,val))

        fenetre.mainloop()

    def ValiderValue(self,event,fen,val):
        self.widget.setValue(val.get(1.0,END).strip())
        self.Quitter(event,fen)

    def modifierResultatAttendu(self):
        fenetre = Toplevel()

        frame = Frame(fenetre)
        frame.pack(side=TOP)
        labelInfo = Label(frame,text="Saisir le resultat attendu :")
        labelInfo.pack(side=TOP)
        val = Text(frame)
        val.pack(side=TOP)
        btnQuit = Button(fenetre,text="Valider")
        btnQuit.pack(side=TOP)
        btnQuit.bind("<1>",lambda event, fen=fenetre, val=val : self.ValiderResultatAttendu(event,fen,val))
        fenetre.mainloop()

    def ValiderResultatAttendu(self,event,fen,val):
        self.widget.setResultatAttendu(val.get(1.0,END).strip())
        self.Quitter(event,fen)

    def modifierAction(self):
        fenetre = Toplevel()

        frame = Frame(fenetre)
        frame.pack(side=TOP)
        labelInfo = Label(frame,text="Attribuer une action : ")
        labelInfo.pack(side=TOP)
        cbb = ttk.Combobox(frame,values=["Montrer une aide","Valider le resultat de l'exercice", "Effacer les réponses"])
        cbb.pack(side=TOP)
        f = Frame(frame)
        f.pack(side=TOP)
        cbb.bind("<<ComboboxSelected>>",lambda event, fen=fenetre,f=f, cbb=cbb: self.action(event,fen,f,cbb))

        fenetre.mainloop()

    def action(self,event, fen, f, cbb):
        self.clean(f)
        act = "cur:" + str(cbb.current()) + "//"
        val = None
        if cbb.current() == 0 :
            
            labelInfo = Label(f,text="Saisir l'aide voulue :")
            labelInfo.pack(side=TOP)
            val = Text(f)
            val.pack(side=TOP)
        else :
            labelInfo = Label(f,text="Prise en compte de l'action : " + cbb.get())
            labelInfo.pack(side=TOP)
            
        btnQuit = Button(fen,text="Valider")
        btnQuit.pack(side=TOP)
        btnQuit.bind("<1>",lambda event, fen=fen,cur=cbb.current(), val=val, act=act : self.ValiderAction(event,fen,val,cur,act))

    

    def ValiderAction(self,event,fen,val,cur,act):
        if cur == 0:
            act = act + val.get(1.0,END)
        self.widget.setActions(act)
        print(self.widget.getActions())
        self.Quitter(event,fen)


    def clean(self,frame):
        for child in frame.winfo_children():
            child.destroy()

    def Quitter(self,event,fen):
        if self.container == None:
            self.sess.update(self.widget)
        else :
            self.sess.update(self.container)
            try:
                self.clean(self.container.frame)
                self.container.showToolBox()
            except:
                pass
        fen.quit()
        fen.destroy()

    def on_delete(self):
        if self.container != None :
            self.sess.remove(self.container.id)
            self.container.destroy()
        else:
            self.sess.remove(self.widget.id)
            self.widget.destroy()

    def clone(self,parent,widget=None):
        if widget == None:
            widget = self.widget
        #parent = self.widget.nametowidget(self.widget.winfo_parent())
        cls = widget.__class__
        clone = cls(widget.type,parent,widget.getMenu())
        for key in widget.configure():
            if key != 'master':
                clone.configure({key: widget.cget(key)})
        return clone