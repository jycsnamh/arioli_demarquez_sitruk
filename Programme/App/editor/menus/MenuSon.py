from tkinter import *
from utils.Bibliotheque import Bibliotheque
from editor.components.items.Son import *
from editor.menus.MenuGen import MenuGen

class MenuSon(MenuGen):
    def __init__(self,root=None,widget=None,event=None,menu=None,sess=None,globs=None,DragManager=None,container = None):
        super().__init__(root,widget,event,menu,sess,globs,DragManager,container)

    # Son
    def Menu(self):
        self.menu.add_command (label = "Selectionner un son dans la bibliothèque" , command=self.modifierSon)
        self.menu.add_command (label = "Modifier le texte du bouton" , command=self.modifierTexteSon)
        self.menu.add_command (label = "Modifier la taille du bouton du son", command=self.modifierTailleSon)
        self.menu.add_command(label="Lire/Arreter le son", command=self.playSon)
        self.menu.add_command (label = "Modifier la valeur associée au son", command=self.modifierValue)
        self.menu.add_separator()
        self.menu.add_command (label = "Supprimer", command=self.on_delete)

    def modifierTexteSon(self,event = None):
        fenetre = Toplevel()
        info = Label(fenetre,text="Appuyez sur 'enter' pour valider.",pady=10)
        info.pack()
        value = StringVar(fenetre)
        value.set("")
        entree = Entry(fenetre, textvariable=value, width=50)
        entree.pack()
        entree.bind("<Return>", lambda event, fen=fenetre : self.changeTextSon(event,fen))
        fenetre.mainloop()

    def changeTextSon(self,event,fen):
        self.widget.config(text=event.widget.get())
        self.Quitter(event,fen)

    def playSon(self,event = None):
        self.widget.getSound().click()

    def modifierSon(self,event = None):
        libSon = Bibliotheque("Son",self.widget,self.sess,self.container)
        

    def modifierTailleSon(self,event = None):
        fenetre = Toplevel()
        self.width = self.widget.winfo_width()
        self.height = self.widget.winfo_height()
        img_X = Label(fenetre,text="Longueur : ")
        img_X.pack()
        sliderXImg = Scale(fenetre,from_=1, to=200 ,orient=HORIZONTAL)
        sliderXImg.pack()
        
        txt_Y = Label(fenetre,text="Hauteur : ")
        txt_Y.pack()
        sliderYImg = Scale(fenetre,from_=1, to=100)
        sliderYImg.pack()

        btn_quit = Button(fenetre,text="Terminer")
        btn_quit.pack()

        sliderXImg.bind("<B1-Motion>", self.changeXSon)
        sliderYImg.bind("<B1-Motion>", self.changeYSon)

        btn_quit.bind("<1>",lambda event,fen=fenetre: self.Quitter(event,fen))

        fenetre.mainloop()

    def changeXSon(self,event):
        self.widget.config(width=event.widget.get())
        

    def changeYSon(self,event):
        self.widget.config(height=event.widget.get())

