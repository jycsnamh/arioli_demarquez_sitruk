from tkinter import *
from tkinter import ttk
import tkinter.font as font
from editor.components.items.Texte import *
from editor.menus.MenuGen import MenuGen



class MenuTexte(MenuGen):
    def __init__(self,root=None,widget=None,event=None,menu=None,sess=None,globs=None,DragManager=None,container = None):
        super().__init__(root,widget,event,menu,sess,globs,DragManager,container)

    #Texte
    def Menu(self):
        #variables Text
        self.tailleTextePolice = 0

        self.menuActions = Menu(self.menu)
        self.menuModifier = Menu(self.menu)
        
        self.menu.add_command (label = "Modifier le texte" , command=self.modifierTexte)
        self.menu.add_command (label = "Modifier la taille de la zone de texte", command=self.modifierTailleZoneTexte)
        self.menu.add_command (label = "Modifier la couleur du texte", command=self.modifierCouleurTexte)
        self.menu.add_command (label = "Modifier la police du texte", command=self.modifierPoliceTexte)
        self.menu.add_command (label = "Modifier le resultat attendu du texte", command=self.modifierResultatAttendu)
        self.menu.add_command (label = "Activer/Désactiver l'édition du texte", command=self.activerModification)
        self.menu.add_separator()
        self.menu.add_command (label = "Supprimer", command=self.on_delete)

    def modifierPoliceTexte(self):
        fonts=list(font.families())
        fonts.sort()
        fenetre = Toplevel()
        
        framePolice = Frame(fenetre)
        framePolice.pack(side=LEFT)
        frameAppliquer = Frame(fenetre)
        frameAppliquer.pack(side=RIGHT)
        
        labelPolice = Label(framePolice,text="Sélectionnez une police :")
        labelPolice.pack()
        cbbPolice = ttk.Combobox(framePolice, values=fonts)
        cbbPolice.current(fonts.index("Arial"))
        cbbPolice.pack()

        labelTaillePolice = Label(framePolice,text="Sélectionnez la taille de la police :")
        labelTaillePolice.pack()
        SliderTaillePolice = Scale(framePolice, from_=1,to=70,orient=HORIZONTAL)
        SliderTaillePolice.pack()

        labelEffet1Police = Label(framePolice,text="Sélectionnez un premier effet :")
        labelEffet1Police.pack()
        cbbEffet1Police = ttk.Combobox(framePolice, values=["normal","bold"])
        cbbEffet1Police.current(1)
        cbbEffet1Police.pack()

        labelEffet2Police = Label(framePolice,text="Sélectionnez un deuxième effet :")
        labelEffet2Police.pack()
        cbbEffet2Police = ttk.Combobox(framePolice, values=["roman","italic"])
        cbbEffet2Police.current(1)
        cbbEffet2Police.pack()
        
        SliderTaillePolice.bind("<B1-Motion>", self.changeTailleTextePolice)

        btn_appliquer = Button(frameAppliquer,text="Appliquer")
        btn_appliquer.pack(side=RIGHT)
        btn_valider = Button(frameAppliquer,text="Terminer")
        btn_valider.pack(side=RIGHT)
        
        btn_appliquer.bind("<1>", lambda event, fen=fenetre, p=cbbPolice.get(), e1=cbbEffet1Police.get(), e2=cbbEffet2Police.get() : self.appliquerPolice(event,fen,p,e1,e2))
        btn_valider.bind("<1>", lambda event, fen=fenetre : self.Quitter(event,fen))
        
        fenetre.mainloop()

    def changeTailleTextePolice(self,event):
        self.tailleTextePolice = event.widget.get()

    def appliquerPolice(self,event,fen,p,e1,e2):
        w = int(self.widget.winfo_width())
        h = int(self.widget.winfo_height())
        police = font.Font(family=p, size=self.tailleTextePolice, weight=e1, slant=e2)
        self.widget.config(font=police)
        self.widget.place(x=self.widget.winfo_rootx(),y=self.widget.winfo_rooty()-23,width=w,height=h)
        
        
    def modifierCouleurTexte(self,event = None):
        listeCouleur=["white","yellow","orange","red","violet","blue","cyan","black"]
        fenetre = Toplevel()
        listeLignesTexte = self.widget.get("1.0",END).split("\n")
        listeLignesTexte.pop(len(listeLignesTexte)-1)
        listeMotsTexte = []
        listeVar = []
        self.listeMots=[]
        checkMot=[]
        coords=[]
        i = 1
        indCheck = 0
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        varAll = IntVar()
        checkAll=Checkbutton( frame, text ="Tous les mots",variable=varAll)
        checkAll.config(state=NORMAL)
        checkAll.config(command=lambda checkAll=checkAll,varAll=varAll, checkMots=checkMot,listeMotsTexte=listeMotsTexte,listeVar=listeVar,coords=coords : self.selectAllTexte(checkAll,varAll,checkMots,listeMotsTexte,listeVar,coords))
        checkAll.pack()
        for ligne in listeLignesTexte :
            l = ligne.split(" ")
            for mot in l :
                if mot != "" and mot != " " and mot != "   ":
                    listeMotsTexte.append(mot)
                    listeVar.append(IntVar())
                    coords.append(str(i)+"."+str(ligne.index(mot)))
                    checkMot.append(Checkbutton( frame, text = mot, variable=listeVar[listeMotsTexte.index(mot)]))
                    checkMot[indCheck].config(state=NORMAL)
                    checkMot[indCheck].pack(side=TOP)
                    checkMot[indCheck].config(command=lambda    cb=checkMot[indCheck],indvar=listeMotsTexte.index(mot),
                                                                lc=coords[listeMotsTexte.index(mot)],
                                                                listeVar=listeVar :
                                                                self.addMot(cb,lc,listeVar,indvar))
                    indCheck = indCheck +1
            i = i + 1
            
        txt_taille = Label(fenetre,text="Couleur du texte : ")
        txt_taille.pack()

        sliderCouleur = Scale(fenetre, from_=0 , to=len(listeCouleur)-1 ,orient=HORIZONTAL)
        sliderCouleur.pack()
        
        btn_valider = Button(fenetre,text="Valider")
        btn_valider.pack()

        sliderCouleur.bind("<B1-Motion>",lambda event,listeCouleur=listeCouleur, listeVar=listeVar, listeMotsTexte=listeMotsTexte : self.changeCouleurTexte(event,listeCouleur,listeVar,listeMotsTexte))
        
        btn_valider.bind("<1>", lambda event, fen=fenetre : self.Quitter(event,fen))

        fenetre.mainloop()

    def selectAllTexte(self,checkAll,varAll,checkMots,listeMotsTexte,listeVar,coords):
        
        if int(checkAll.getvar(str(varAll))) == 1 :
            for cb in checkMots :
                cb.select()
                lc = coords[listeMotsTexte.index(cb.cget('text'))]
                indvar = listeMotsTexte.index(cb.cget('text'))
                self.addMot(cb,lc,listeVar,indvar)
        else :
            for cb in checkMots :
                cb.deselect()
                lc = coords[listeMotsTexte.index(cb.cget('text'))]
                indvar = listeMotsTexte.index(cb.cget('text'))
                self.addMot(cb,lc,listeVar,indvar)

    def addMot(self,cb,lc,listeVar,indvar):
        if int(cb.getvar(str(listeVar[indvar]))) == 1 and lc + "/" + cb.cget('text') not in self.listeMots :
            self.listeMots.append(lc + "/" + cb.cget('text') )
        elif int(cb.getvar(str(listeVar[indvar])))  == 0 :
            self.listeMots.remove(lc + "/" + cb.cget('text') )
    
    def changeCouleurTexte(self,event,listeCouleur,listeVar,listeMotsTexte):
        for elem in self.listeMots :
            (coord,mot) = elem.split("/")
            (l,c) = coord.split(".")
            self.widget.tag_add("color "+mot, coord, l + "."+ str(int(c) + len(mot)+1))
            self.widget.tag_config("color "+mot, foreground=listeCouleur[event.widget.get()])
            for tag in self.widget.tag:
                if tag.find("color "+mot) != -1:
                    self.widget.tag.remove(tag)
            self.widget.tag.append("add::name="+"color "+mot + "//coord,:start="+coord+"/end="+ l + "."+ str(int(c) + len(mot)+1)+"//foreground="+listeCouleur[event.widget.get()])

    def modifierTailleZoneTexte(self,event = None):
        fenetre = Toplevel()
        
        txt_X = Label(fenetre,text="Longueur : ")
        txt_X.pack()
        sliderXTexte = Scale(fenetre,from_=1, to=200 ,orient=HORIZONTAL)
        sliderXTexte.pack()
        
        txt_Y = Label(fenetre,text="Hauteur : ")
        txt_Y.pack()
        sliderYTexte = Scale(fenetre,from_=1, to=50)
        sliderYTexte.pack()

        btn_valider = Button(fenetre,text="Valider")
        btn_valider.pack()

        sliderXTexte.bind("<B1-Motion>", self.changeXTexte)
        sliderYTexte.bind("<B1-Motion>", self.changeYTexte)

        btn_valider.bind("<1>", lambda event, fen=fenetre : self.Quitter(event,fen))
        fenetre.mainloop()
        
    def changeXTexte(self,event):
        self.widget.config(width=event.widget.get())
        

    def changeYTexte(self,event):
        self.widget.config(height=event.widget.get())
        

    def modifierTexte(self,event = None):
        fenetre = Toplevel()
        texte = self.clone(fenetre)
        texte.configure(state=NORMAL)
        self.setTextInput(texte,self.widget.get("1.0",END))
        texte.pack()
        btn_valider= Button(fenetre,text="Valider")
        btn_valider.pack()

        btn_valider.bind("<1>",lambda event,fen=fenetre,texte=texte : self.validerModificationTexte(event,fen,texte))
        
        fenetre.mainloop()

    def validerModificationTexte(self,event,fen,texte):
        for key in texte.configure():
            self.widget.configure({key: texte.cget(key)})
        self.setTextInput(self.widget,texte.get("1.0",END).strip())
        self.widget.configure(state=DISABLED)
        self.Quitter(event,fen)

    def setTextInput(self,widget,text):
        widget.delete(1.0,"end")
        widget.insert(1.0, text)

    def changeTexte(self,event, fen):
        self.widget.config(text=event.widget.get())
        
    def activerModification(self):
        if self.widget.active == "DISABLED":
            self.widget.active = "NORMAL"
        else :
            self.widget.active = "DISABLED"
        if self.container != None :
            self.sess.update(self.container)
        else:
            self.sess.update(self.widget)

    