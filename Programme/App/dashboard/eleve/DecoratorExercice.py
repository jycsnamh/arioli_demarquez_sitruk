from tkinter import *
from utils.Interpreter import *

class DecoratorExercice:
    def __init__(self,root,parent,sess,globs,exo):
        self.parent = parent
        self.root = root
        self.globs = globs
        self.sess = sess
        self.exercice = exo
        self.sess.load(self.exercice["idExercice"])
        self.suivant =None
        self.interpreter = Interpreter(self.globs)

    def setSuivant(self,suivant):
        self.suivant = suivant

    def getSuivant(self):
        return self.suivant

    def recap(self):
        f = Frame(self.root)
        f.pack(side=TOP)
        btnExercice = Button(f,text=self.exercice["nom"],width=20,height=3)
        btnExercice.pack(side=LEFT)
        # labelDifficulte = Label(f,text=self.exercice["difficulte"],fg="blue")
        # labelDifficulte.pack(side=RIGHT)
        btnExercice.bind("<1>",self.exec)

    def load(self):
        self.interpreter.load(self.root,self.sess,False,"Exec",RIGHT)

    def exec(self,event = None):
        self.clean(self.root)
        btn_retour = Button(self.root,text="Retour",width=30,height=3,bg="grey")
        btn_retour.pack(anchor=NE)
        btn_retour.bind("<1>", self.parent.dashboard)
        self.load()
        if self.suivant != None :
            btn_continue = Button(self.root,text="Continuer")
            btn_continue.pack(anchor=SE)
            btn_continue.bind("<1>", self.getSuivant().exec)
        else : 
            btn_finish = Button(self.root,text="Terminer")
            btn_finish.pack(anchor=SE)
            btn_finish.bind("<1>", self.parent.dashboard)

    def clean(self,frame):
        for child in frame.winfo_children():
            child.destroy()