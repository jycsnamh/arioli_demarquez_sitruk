import datetime
from tkinter import *
from editor.Editor import Editor
from dashboard.eleve.DecoratorExercice import DecoratorExercice
from editor.components.items.Bouton import Bouton
from editor.components.containers.ToolBox import *
from editor.menus.MenuBouton import *
from editor.menus.MenuContainer import *
from editor.menus.MenuForme import *
from services.Eleves_services import *
from utils.Table import Table
from utils.Interpreter import *
from utils.Globs import *
from utils.Sess import *

class DashBoardEleve:
    def __init__(self,base,root,token,service):
        self.base = base
        self.root = root
        self.token = token
        self.services = service
        self.globs = Globs()
        self.selected = None
        self.iPage = 0
        self.iExo = 0

        self.globs.createGlobs("listeDrop", Editor.listeDrop)
        
        # initialisation des variables globales sur les types de menu
        menuBouton = MenuBouton
        self.globs.createGlobs(str(menuBouton), menuBouton)
        menuContainer = MenuContainer
        self.globs.createGlobs(str(menuContainer), menuContainer)
        menuForme = MenuForme
        self.globs.createGlobs(str(menuForme), menuForme)
        menuImage = MenuImage
        self.globs.createGlobs(str(menuImage), menuImage)
        menuSon = MenuSon
        self.globs.createGlobs(str(menuSon), menuSon)
        menuTexte = MenuTexte
        self.globs.createGlobs(str(menuTexte), menuTexte)
        # menuZoneDessin = MenuZoneDessin
        # self.globs.createGlobs(str(menuZoneDessin), menuZoneDessin)
        

        # initialisation des variables globales sur les types des composants
        self.genBouton = Bouton("Button",self.root,menu=self.globs.getValue(str(menuBouton)), text="Nouveau bouton",relief=RAISED)
        self.globs.createGlobs(str(self.genBouton.__class__), self.genBouton.__class__)
        self.genImage = Img("Label",self.root,menu=self.globs.getValue(str(menuImage)), bg="black",width=30,height=10)
        self.globs.createGlobs(str(self.genImage.__class__), self.genImage.__class__)
        self.genTexte = Texte("Text",self.root,menu=self.globs.getValue(str(menuTexte)))
        self.globs.createGlobs(str(self.genTexte.__class__), self.genTexte.__class__)
        self.genContainer = ContainerComponent("Frame",self.root,menu=self.globs.getValue(str(menuContainer)), bg="black",width=280,height=150,sess=None)
        self.globs.createGlobs(str(self.genContainer.__class__), self.genContainer.__class__)
        self.genSon = Son("Button",self.root,menu=self.globs.getValue(str(menuSon)),text="Nouveau son")
        self.globs.createGlobs(str(self.genSon.__class__), self.genSon.__class__)
        self.genToolBox = ToolBox(self.root,sess=None,globs=self.globs)
        self.globs.createGlobs(str(self.genToolBox.__class__), self.genToolBox.__class__)

        self.dashboard()

    def reinitIExo(self):
        self.iExo = 0

    def reinitIPage(self):
        self.iPage = 0

    def dashboard(self,event = None):
        self.base.clean()
        frameInfo = Frame(self.root)
        frameInfo.pack()
        
        label_infoDateJour = Label(frameInfo,text="Nous sommes le " + str(datetime.datetime.utcnow().strftime("%d/%m/%Y %H:%M:%S")))
        label_infoDateJour.pack(side=LEFT)
        btnDeco = Button(frameInfo,text="Déconnexion",width=30,height=3)
        btnDeco.pack(anchor=NE)
        
        frame = Frame(self.root)
        frame.place(x=(self.root.winfo_width()/2), y=(self.root.winfo_height()/3))
        frameBtn = Frame(frame)
        frameBtn.pack(side=TOP)
        btn_exercicesAFaire = Button(frameBtn, text="Exercices",width=30,height=3)
        btn_exercicesAFaire.pack(side=LEFT)
        # btn_statistiques = Button(frameBtn, text="Statistiques",width=30,height=3)
        # btn_statistiques.pack(side=RIGHT)
        # btn_historique = Button(frameBtn, text="Historique",width=30,height=3)
        # btn_historique.pack(side=RIGHT)
        self.framePage = Frame(frame)
        self.framePage.pack(side=BOTTOM)
        btn_exercicesAFaire.bind("<1>",self.listPages)
        # btn_historique.bind("<1>",self.historique)
        # btn_statistiques.bind("<1>",self.statistiques)
        btnDeco.bind("<1>",self.base.main)

    # def historique(self,event):
    #     self.base.clean()
    #     btnRetour = Button(self.root,text="Retour", width=30,height=3,bg="grey")
    #     btnRetour.pack(anchor=NE)
    #     btnRetour.bind("<1>",self.dashboard)

    # def statistiques(self,event):
    #     self.base.clean()
    #     btnRetour = Button(self.root,text="Retour", width=30,height=3,bg="grey")
    #     btnRetour.pack(anchor=NE)
    #     btnRetour.bind("<1>",self.dashboard)

    def listPages(self,event=None):
        self.base.clean()
        btnRetour = Button(self.root,text="Retour", width=30,height=3,bg="grey")
        btnRetour.pack(anchor=NE)
        btnRetour.bind("<1>",self.dashboard)
        frame = Frame(self.root)
        frame.place(x=(self.root.winfo_width()/2) - 400, y=(self.root.winfo_height()/3))
        
        allWork = self.services.getPagesToDo(self.token)
        listePages=[]
        suivant = None
        for keyPage , page in allWork.items():
            for keyExo, exo in page["exercices"].items():
                sess = Sess(self.root,self.token,exo["idExercice"],exo["nom"],False)
                decoExo = DecoratorExercice(self.root,self,sess,self.globs,exo)
                decoExo.setSuivant(suivant)
                suivant = decoExo
                decoExo.recap()
           
        btn_start = Button(self.root,text="Faire toutes les pages")
        btn_start.pack(side=BOTTOM)
        btn_start.bind("<1>", decoExo.exec)
