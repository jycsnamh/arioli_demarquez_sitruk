from tkinter import *
from dashboard.prof.ClasseVirtuelle import *

class Dashboard:
    def __init__(self, base, root, token,parent):
        self.root = root
        self.base = base
        self.token = token
        self.parent = parent
        self.MainPage()

    def quitter(self, event, fen):
        fen.quit()
        fen.destroy()

    def MainPage(self, event=None):
        self.base.clean()
        frameDeconnexion = Frame(self.root)
        frameMain = Frame(self.root)
        bouttonQuitte = Button(frameDeconnexion, text="Quitter")
        bouttonDeco = Button(frameDeconnexion, text="Déconnexion")
        bouttonClasse = Button(frameMain, text="Classes Virtuelles")
        bouttonPages = Button(frameMain, text="Pages")
        bouttonExercices = Button(frameMain, text="Exercices")
        
        frameDeconnexion.pack(side=TOP)
        frameMain.pack(side=TOP)

        bouttonClasse.pack(side=TOP)
        bouttonPages.pack(side=TOP)
        bouttonExercices.pack(side=TOP)
        bouttonDeco.pack(anchor=NE,side=LEFT)
        bouttonQuitte.pack(anchor=NE,side=RIGHT)

        bouttonClasse.bind("<1>", self.ClassesVirtuelles)
        bouttonPages.bind("<1>", self.Pages)
        bouttonExercices.bind("<1>", self.Exercices)
        bouttonDeco.bind("<1>", self.deconnexion)
        bouttonQuitte.bind("<1>", self.base.Quitter)

    def deconnexion(self,event):
        popup = Toplevel()
        label = Label(popup,text="Vous voulez vous déconnecter ?")
        label.pack(side=TOP)
        frameBtn = Frame(popup)
        frameBtn.pack(side=TOP)
        frameDeco = Frame(frameBtn)
        frameDeco.pack(side=TOP)
        btnDecoMain = Button(frameDeco,text="Retour vers l'accueil")
        btnDecoMain.pack(side=LEFT)
        btnDecoCo = Button(frameDeco,text="Retour vers l'écran de connexion")
        btnDecoCo.pack(side=RIGHT)
        btnAnnuler = Button(frameBtn,text="Annuler")
        btnAnnuler.pack(side=TOP)

        btnDecoMain.bind("<1>", self.base.main)
        btnDecoCo.bind("<1>", self.parent.co)
        btnAnnuler.bind("<1>",lambda event, fen=popup : self.Quitter(event,fen))

        popup.mainloop()

    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()

    def ClassesVirtuelles(self,event):
        ClasseVirtuellePage=ClasseVirtuelle(self.base, self.root, self.token, self)

    def Pages(self,event):
        pages = Page(self.base,self.root,self.token,self)

    def Exercices(self,event):
        exercices = Exercice(self.base,self.root,self.token,self)