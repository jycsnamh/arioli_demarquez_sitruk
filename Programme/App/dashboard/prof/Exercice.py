from tkinter import *
from tkinter import ttk

from services.Exercices_services import *
from dashboard.prof.Management import Management

class Exercice :
    def __init__(self,base,root,token,parent,idPage=-1,fromManagement = False):
        self.parent = parent
        self.base = base
        self.root = root
        self.token = token
        self.idPage = idPage
        self.fromManagement = fromManagement
        self.service = Exercices_services(self.token)
        self.MainPage()

    def MainPage(self,event = None):
        self.base.clean()
        BouttonRetour=Button(self.root, text="Retour",width=30,height=3,bg="#cccccc")
        BouttonRetour.pack(anchor=NW)
        BouttonRetour.bind("<1>", self.parent.MainPage)

        frameExercices = Frame(self.root)
        frameExercices.pack(side=TOP)
        
        self.frameListeExercices = Frame(frameExercices)
        self.frameListeExercices.pack(side=TOP)
        
        self.listeExercices(self.frameListeExercices)

        BouttonCreation=Button(self.root, text="Créer un exercice",width=30,height=3,bg="orange")
        BouttonCreation.pack(side=BOTTOM)

        BouttonCreation.bind("<1>",lambda event : self.popupCreerPage(event))

    def popupCreerPage(self,event):
        fenetre = Toplevel()
        # nom
        label = Label(fenetre,text="Saisir le nom de l'exercice : ")
        label.pack(side=TOP)
        var = StringVar(fenetre)
        var.set("")
        entreeNomExercice = Entry(fenetre,textvariable=var)
        entreeNomExercice.pack(side=TOP)
        label = Label(fenetre,text="Entrez la difficulté de l'exercice : ")
        label.pack(side=TOP)
        cbbDificulte = ttk.Combobox(fenetre, values=["Facile","Moyen","Difficile"])
        cbbDificulte.current(0)
        cbbDificulte.pack(side=TOP)
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        btn_valider = Button(frame,text="Valider")
        btn_valider.pack(side=RIGHT)
        btn_annuler= Button(frame,text="Annuler")
        btn_annuler.pack(side=LEFT)

        btn_valider.bind("<1>",lambda event, fen=fenetre,entry=entreeNomExercice,difficulte = cbbDificulte : self.creerExercice(event,fen,entry,var,difficulte))
        btn_annuler.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))

        fenetre.mainloop()


    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()

    def creerExercice(self,event,fen,entryNom,varNom,varDifficulte):
        self.service.creerExercice(varNom.get(),varDifficulte.get())
        for child in self.frameListeExercices.winfo_children():
            child.destroy()
        self.listeExercices(self.frameListeExercices)
        self.Quitter(event,fen)

    def listeExercices(self,frame):
        Exercices = self.service.getExercices()
        label = Label(frame,text="Liste des exercices",font=("Arial",40))
        label.pack(side=TOP)
        if Exercices != None and len(Exercices)> 0 :
            for key,exercice in Exercices.items():
                frameExercice = Frame(frame)
                frameExercice.pack(side=TOP)
                nomExercice = Label(frameExercice,text="Exercice : " + exercice["nom"] + "   ",font=("Arial",20))
                nomExercice.pack(side=LEFT)
                BouttonManagement=Button(frameExercice, text="Management",bg="cyan")
                BouttonManagement.pack(side=RIGHT)
                # script = None
                # if "script" in exercice :
                #     script = exercice["script"]
                BouttonManagement.bind("<1>",lambda event, idExercice=exercice["idExercice"],nomExercice=exercice["nom"] : self.management(event,"Exercice",idExercice,nomExercice))
        else : 
            label_listeExercicesEmpty = Label(frame,text="Aucun Exercice....",font=("Arial",30))
            label_listeExercicesEmpty.pack(side=TOP)


    def management(self, event,Type,idExercice,nomExercice):
        
        managementExercice = Management(self.base, self.root, self.token, self,self.service,Type,idExercice,nomExercice,self.idPage)
        
