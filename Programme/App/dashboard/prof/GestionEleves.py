from tkinter import *

class GestionEleves:
    def __init__(self,id,service) :
        self.service = service
        self.id = id
        self.fenetre = Toplevel()
        
        self.page()

        self.fenetre.mainloop()

    def page(self,event =None):
        self.clean()
        self.listeEleves()
        frame = Frame(self.fenetre)
        frame.pack(side=TOP)
        btnAjouterEleve = Button(frame,text="Ajouter un élève")
        btnAjouterEleve.pack(side=LEFT)
        
        btn_fermer = Button(frame, text="Fermer")
        btn_fermer.pack(side=RIGHT)

        btnAjouterEleve.bind("<1>", self.addEleve)
        btn_fermer.bind("<1>",self.Quitter)

    def addEleve(self,event):
        self.clean()
        labelNomEleve = Label(self.fenetre,text="Ajouter le prénom de l'élève :")
        labelNomEleve.pack(side=TOP)
        var = StringVar()
        var.set("")
        entryNom = Entry(self.fenetre,textvariable=var)
        entryNom.pack(side=TOP)
        frame = Frame(self.fenetre)
        frame.pack(side=TOP)
        btnValider = Button(frame,text="Valider")
        btnAnnuler = Button(frame,text="Annuler")
        btnValider.pack(side=RIGHT)
        btnAnnuler.pack(side=LEFT)
        btnValider.bind("<1>",lambda event, var=var,entry=entryNom : self.validerAjoutEleve(event,var,entry))
        btnAnnuler.bind("<1>",self.page)

    def validerAjoutEleve(self,event,var,entry):
        self.service.addEleve(self.id,var.get())
        self.page()

    def listeEleves(self):
        eleves = self.service.getEleves(self.id)
        labelListe = Label(self.fenetre,text="Liste des élèves",font=("Arial",16))
        labelListe.pack(side=TOP)
        if len(eleves) > 0 :
            for key,eleve in eleves.items():
            
                frameEleve = Frame(self.fenetre)
                frameEleve.pack(side=TOP)
                labelNomEleve = Label(frameEleve,text=eleve["nom"])
                labelNomEleve.pack(side=LEFT)
                btnSupprimerEleves = Button(frameEleve,text="Supprimer")
                btnSupprimerEleves.pack(side=RIGHT)

                btnSupprimerEleves.bind("<1>",lambda event, nom=eleve["nom"] : self.supprimerEleve(event,nom))
        else :
            labelListeEleveEmpty = Label(self.fenetre,text="Aucun élève...",font=("Arial",30))
            labelListeEleveEmpty.pack(side=TOP)

    def supprimerEleve(self,event,nom):
        self.service.deleteEleve(self.id,nom)
        self.page()

    def Quitter(self,event):
        self.fenetre.quit()
        self.fenetre.destroy()
    
    def clean(self):
        for child in self.fenetre.winfo_children():
            child.destroy()