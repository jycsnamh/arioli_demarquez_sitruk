from tkinter import *
from dashboard.prof.Management import Management
from services.Classes_services import *
from dashboard.prof.Page import *
from dashboard.prof.Code import *

class ClasseVirtuelle:
    def __init__(self,base, root, token, parent):
        self.base = base
        self.parent = parent
        self.root = root
        self.token = token
        self.service = Classes_services(self.token)
        self.MainPage()

    def MainPage(self,event = None):
        self.base.clean()
        BouttonRetour=Button(self.root, text="Retour",width=30,height=3,bg="#cccccc")
        BouttonRetour.pack(anchor=NW)
        BouttonRetour.bind("<1>", self.parent.MainPage)

        frameClasses = Frame(self.root)
        frameClasses.pack(side=TOP)
        
        frameListeClasses = Frame(frameClasses)
        frameListeClasses.pack(side=TOP)
        
        self.listeClasses(frameListeClasses)

        BouttonCreation=Button(self.root, text="Créer une classe",width=30,height=3,bg="orange")
        BouttonCreation.pack(side=BOTTOM)

        BouttonCreation.bind("<1>",lambda event, frameUpdate=frameListeClasses : self.popupCreerClasse(event,frameUpdate))
        
    def popupCreerClasse(self,event,frameUpdate):
        fenetre = Toplevel()
        # nom
        label = Label(fenetre,text="Saisir le nom de la classe : ")
        label.pack(side=TOP)
        var = StringVar(fenetre)
        var.set("")
        entreeNomClasse = Entry(fenetre,textvariable=var)
        entreeNomClasse.pack(side=TOP)
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        btn_valider = Button(frame,text="Valider")
        btn_valider.pack(side=RIGHT)
        btn_annuler= Button(frame,text="Annuler")
        btn_annuler.pack(side=LEFT)

        btn_valider.bind("<1>",lambda event, fen=fenetre,entry=entreeNomClasse,var=var,frameUpdate=frameUpdate : self.creerClasse(event,fen,entry,var,frameUpdate))
        btn_annuler.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))

        fenetre.mainloop()


    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()

    def creerClasse(self,event,fen,entry,var,frameUpdate):
        self.service.creerClasse(var.get())
        for child in frameUpdate.winfo_children():
            child.destroy()
        self.listeClasses(frameUpdate)
        self.Quitter(event,fen)

    def listeClasses(self,frame):
        classes = self.service.getClasses()
        label = Label(frame,text="Liste des classes virtuelles",font=("Arial",40))
        label.pack(side=TOP)
        if classes != None and len(classes)> 0 :
            for key,classe in classes.items():
                frameClasse = Frame(frame)
                frameClasse.pack(side=TOP)
                nomClasse = Label(frameClasse,text="Classe : " + classe["nom"] + "   ",font=("Arial",20))
                nomClasse.pack(side=LEFT)
                BouttonManagement=Button(frameClasse, text="Management",bg="cyan")
                BouttonManagement.pack(side=RIGHT)

                BouttonManagement.bind("<1>",lambda event, idClasse=classe["idClasse"],nomClasse=classe["nom"] : self.management(event,"Classe",idClasse,nomClasse))
        else : 
            label_listeClassesEmpty = Label(frame,text="Aucune classe....",font=("Arial",30))
            label_listeClassesEmpty.pack(side=TOP)


    def management(self, event,Type,idClasse,nomClasse):
        managementClasse = Management(self.base, self.root, self.token, self, self.service, Type, idClasse, nomClasse,-1,Page)
        
