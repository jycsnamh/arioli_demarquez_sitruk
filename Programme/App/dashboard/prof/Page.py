from tkinter import *
from dashboard.prof.Exercice import *
from dashboard.prof.Management import Management
from services.Pages_services import *

class Page:
    def __init__(self,base, root, token, parent,idClasse = -1,fromManagement = False):
        self.parent = parent
        self.base = base
        self.root = root
        self.token = token
        self.idClasse = idClasse
        self.fromManagement = fromManagement
        self.service = Pages_services(self.token)
        self.MainPage()

    def MainPage(self,event = None):
        self.base.clean()
        BouttonRetour=Button(self.root, text="Retour",width=30,height=3,bg="#cccccc")
        BouttonRetour.pack(anchor=NW)
        BouttonRetour.bind("<1>", self.parent.MainPage)

        framePages = Frame(self.root)
        framePages.pack(side=TOP)
        
        frameListePages = Frame(framePages)
        frameListePages.pack(side=TOP)
        
        self.listePages(frameListePages)

        BouttonCreation=Button(self.root, text="Créer une page",width=30,height=3,bg="orange")
        BouttonCreation.pack(side=BOTTOM)

        BouttonCreation.bind("<1>",lambda event, frameUpdate=frameListePages : self.popupCreerPage(event,frameUpdate))

    def popupCreerPage(self,event,frameUpdate):
        fenetre = Toplevel()
        # nom
        label = Label(fenetre,text="Saisir le nom de la page : ")
        label.pack(side=TOP)
        var = StringVar(fenetre)
        var.set("")
        entreeNomPage = Entry(fenetre,textvariable=var)
        entreeNomPage.pack(side=TOP)
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        btn_valider = Button(frame,text="Valider")
        btn_valider.pack(side=RIGHT)
        btn_annuler= Button(frame,text="Annuler")
        btn_annuler.pack(side=LEFT)

        btn_valider.bind("<1>",lambda event, fen=fenetre,entry=entreeNomPage,var=var,frameUpdate=frameUpdate : self.creerPage(event,fen,entry,var,frameUpdate))
        btn_annuler.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))

        fenetre.mainloop()


    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()

    def creerPage(self,event,fen,entryNom,varNom,frameUpdate):
        self.service.creerPage(varNom.get())
        for child in frameUpdate.winfo_children():
            child.destroy()
        self.listePages(frameUpdate)
        self.Quitter(event,fen)

    def listePages(self,frame):
        pages = self.service.getPages()
        label = Label(frame,text="Liste des pages d'exercices",font=("Arial",40))
        label.pack(side=TOP)
        if pages != None and len(pages)> 0 :
            for key,page in pages.items():
                framePage = Frame(frame)
                framePage.pack(side=TOP)
                nomPage = Label(framePage,text="Page : " + page["nom"] + "   ",font=("Arial",20))
                nomPage.pack(side=LEFT)
                BouttonManagement=Button(framePage, text="Management",bg="cyan")
                BouttonManagement.pack(side=RIGHT)

                BouttonManagement.bind("<1>",lambda event, idPage=page["idPage"],nomPage=page["nom"] : self.management(event,"Page",idPage,nomPage))
        else : 
            label_listePagesEmpty = Label(frame,text="Aucune page....",font=("Arial",30))
            label_listePagesEmpty.pack(side=TOP)


    def management(self, event,Type,idPage,nomPage):    
        managementClasse = Management(self.base, self.root, self.token, self, self.service, Type, idPage, nomPage, self.idClasse, Exercice)
        
