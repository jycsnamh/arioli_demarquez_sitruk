from tkinter import *

from utils.InfoBulle import InfoBulle

class Code :
    def __init__(self,idClasse,service):
        self.id = idClasse
        self.service = service
        fenetre = Toplevel()

        frame = Frame(fenetre)
        frame.pack(side=TOP)
        labelInfo = Label(frame,text="")
        self.info = InfoBulle(frame,labelInfo)
        btn_genererCode = Button(frame,text="Générer un nouveau code")
        btn_genererCode.pack(side=LEFT)
        btn_afficherCode = Button(frame,text="Afficher le code de la classe")
        btn_afficherCode.pack(side=RIGHT)
        btn_fermer = Button(fenetre, text="Fermer")
        btn_fermer.pack(side=BOTTOM)

        btn_genererCode.bind("<1>", self.genererCode)
        btn_afficherCode.bind("<1>", self.afficherCode)
        btn_fermer.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))

        fenetre.mainloop()

    def genererCode(self,event):
        self.info.affiche(self.service.genererCode(self.id))

    def afficherCode(self,event):
        fenetre = Toplevel()
        labelCode = Label(fenetre,text="Code : " + self.service.getCode(self.id))
        labelCode.pack(side=TOP)
        btn_fermer = Button(fenetre, text="Fermer")
        btn_fermer.pack(side=BOTTOM)
        btn_fermer.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        fenetre.mainloop()

    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()