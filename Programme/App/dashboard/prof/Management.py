from tkinter import *
from tkinter import font
from services.Pages_services import Pages_services
from services.Exercices_services import Exercices_services
from dashboard.prof.GestionEleves import GestionEleves
from dashboard.prof.Code import Code
from editor.Editor import *

class Management:
    def __init__(self,base, root, token, parent,service,Type,id,nom,idContainer,futurContenu=None,script = None):
        self.service = service
        self.futurContenu = futurContenu
        self.idContainer = idContainer
        self.id = id
        self.nom = nom
        self.type = Type
        self.base = base
        self.root = root
        self.token = token
        self.parent = parent
        self.MainPage()

    def modifierManagement(self,Type,parent,service,idContainer,futurContenu,script=None):
        self.service = service
        self.futurContenu = futurContenu
        self.idContainer = idContainer
        self.type = Type
        self.parent = parent

    def MainPage(self,event = None):
        if self.type == "Classe":
            self.managementClasse()
        elif self.type == "Page" :
            self.managementPage()
        elif self.type == "Exercice" :
            self.managementExercice()
        
    def init(self):
        self.base.clean()
        BouttonRetour=Button(self.root, text="Retour",width=30,height=3,bg="#cccccc")
        BouttonRetour.pack(anchor=NW)
        BouttonRetour.bind("<1>", self.parent.MainPage)
        label = Label(self.root,text=self.type + " management : " + self.nom,font=("Arial",40))
        label.pack(side=TOP)

    def managementClasse(self):
        self.init()
        frame = Frame(self.root)
        BouttonAjoutPages = Button(frame,text="Ajouter des pages")
        BouttonAjoutPages.pack(side=LEFT)
        BouttonGestionEleves = Button(frame,text="Gérer les élèves")
        BouttonGestionEleves.pack(side=LEFT)
        BouttonCode=Button(frame, text="Code d'accès")
        BouttonCode.pack(side=LEFT)
        BouttonPages=Button(frame, text="Gestion des pages")
        BouttonPages.pack(side=LEFT)
        BouttonSupression=Button(frame, text="Supprimer la classe")
        BouttonSupression.pack(side=RIGHT)
        frame.place(x=(self.root.winfo_width()/2) - 200, y=(self.root.winfo_height()/2) -  100)

        BouttonAjoutPages.bind("<1>", self.ajoutPages)
        BouttonGestionEleves.bind("<1>", self.gestionEleves)
        BouttonCode.bind("<1>", self.sectionCode)
        BouttonPages.bind("<1>", self.sectionFuturContenu)
        BouttonSupression.bind("<1>", self.supprimer)

    def ajoutPages(self,event):          
        fenetre = Toplevel() 
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        self.listePages(frame)
        BouttonAjout=Button(fenetre, text="Ajouter des pages",bg="cyan",width=30,height=3)
        BouttonAjout.pack(side=LEFT)
        BouttonRetirer=Button(fenetre, text="Retirer des pages",bg="cyan",width=30,height=3)
        BouttonRetirer.pack(side=LEFT)
        BouttonQuit=Button(fenetre, text="Fermer",bg="cyan",width=30,height=3)
        BouttonQuit.pack(side=RIGHT)
        frameInfo = Frame(fenetre)
        frameInfo.pack(side=BOTTOM)
        labelInfo = Label(frameInfo,text="")
        self.info = InfoBulle(frameInfo,labelInfo)
        BouttonQuit.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        BouttonAjout.bind("<1>",lambda event, fr=frame : self.ajouterPages(event,fr))
        BouttonRetirer.bind("<1>",lambda event, fr=frame : self.retirerPages(event,fr))
        fenetre.mainloop()

    def retirerPages(self, event, fr):
        pagesAjoutes = self.service.getPages(self.id)
        fenetre = Toplevel() 
        label = Label(fenetre,text="Liste des pages à retirer de la classe :" + self.nom,font=("Arial",30))
        label.pack(side=TOP)
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        self.listeRemovePages(frame)
        BouttonQuit=Button(fenetre, text="Fermer",bg="cyan",width=30,height=3)
        BouttonQuit.pack(side=BOTTOM)
        BouttonQuit.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        fenetre.mainloop()
        self.listePages(fr)


    def listeRemovePages(self, frame):
        Pages = self.service.getPages(self.id)
        if Pages != None and len(Pages)> 0 :
            for key,page in Pages.items():
                framePage = Frame(frame)
                framePage.pack(side=TOP)
                nomPage = Label(framePage,text="Page : " + page["nom"] + "   ",font=("Arial",20))
                nomPage.pack(side=LEFT)
                if len(Pages) > 0 :
                    BouttonAjout=Button(framePage, text="Retirer",bg="cyan")
                    BouttonAjout.pack(side=RIGHT)
                    BouttonAjout.bind("<1>",lambda event, idPage=page["idPage"],nomPage=page["nom"] : self.removePage(event,frame,idPage,nomPage))
        else : 
            label_listePagesEmpty = Label(frame,text="Aucune pages ...",font=("Arial",30))
            label_listePagesEmpty.pack(side=TOP)    


    def ajouterPages(self,event, fr):
        fenetre = Toplevel() 
        label = Label(fenetre,text="Liste des pages à ajouter à la classe :" + self.nom,font=("Arial",30))
        label.pack(side=TOP)
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        self.listeAddPages(frame)
        BouttonQuit=Button(fenetre, text="Fermer",bg="cyan",width=30,height=3)
        BouttonQuit.pack(side=BOTTOM)
        BouttonQuit.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        fenetre.mainloop()
        self.listePages(fr)

    def listeAddPages(self, frame):
        self.clean(frame)
        servicePages = Pages_services(self.token)
        Pages = servicePages.getPages()
        PagesAjoutes = self.service.getPages(self.id)
        if Pages != None and len(Pages)> 0 :#and len(Pages) > len(PagesAjoutes)  :
            for key,page in Pages.items():
                framePage = Frame(frame)
                framePage.pack(side=TOP)
                if len(PagesAjoutes) > 0 :
                    b = False
                    for ke,ex in PagesAjoutes.items():
                        if ex["idPage"] == page["idPage"]:
                            b = True
                    if not b:
                        nomPage = Label(framePage,text="Page : " + page["nom"] + "   ",font=("Arial",20))
                        nomPage.pack(side=LEFT)
                        BouttonAjout=Button(framePage, text="Ajouter",bg="cyan")
                        BouttonAjout.pack(side=RIGHT)
                        BouttonAjout.bind("<1>",lambda event, idPage=page["idPage"],nomPage=page["nom"] : self.ajoutPage(event,frame,idPage,nomPage))
                else :
                    nomPage = Label(framePage,text="Page : " + page["nom"] + "   ",font=("Arial",20))
                    nomPage.pack(side=LEFT)
                    BouttonAjout=Button(framePage, text="Ajouter",bg="cyan")
                    BouttonAjout.pack(side=RIGHT)
                    BouttonAjout.bind("<1>",lambda event, idPage=page["idPage"],nomPage=page["nom"] : self.ajoutPage(event,frame,idPage,nomPage))
        else : 
            label_listeExercicesEmpty = Label(frame,text="Aucune pages....",font=("Arial",30))
            label_listeExercicesEmpty.pack(side=TOP)


    def listePages(self,frame):
        self.clean(frame)
        pagesAjoutes = self.service.getPages(self.id)
        label = Label(frame,text="Liste des pages de la classe :" + self.nom,font=("Arial",30))
        label.pack(side=TOP)
        if pagesAjoutes != None and len(pagesAjoutes)> 0 :
            for key,page in pagesAjoutes.items():
                framePage = Frame(frame)
                framePage.pack(side=TOP)
                nomPage = Label(framePage,text="Page : " + page["nom"] + "   ",font=("Arial",20))
                nomPage.pack(side=LEFT)
        else : 
            label_listePagesEmpty = Label(frame,text="Aucune page....",font=("Arial",30))
            label_listePagesEmpty.pack(side=TOP)

    def removePage(self,event,frame,idPage,nomPage):
            self.clean(frame)
            message = self.service.removePage(self.id,idPage,nomPage)
            self.info.affiche(message)
            self.listeRemovePages(frame)
    
            
    def ajoutPage(self,event,frame,idPage,nomPage):
        self.clean(frame)
        message = self.service.addPage(self.id,idPage,nomPage)
        self.info.affiche(message)
        self.listeAddPages(frame)


    def gestionEleves(self,event):
        popup = GestionEleves(self.id,self.service)

    def sectionCode(self,event):
        popup = Code(self.id,self.service)

    def sectionFuturContenu(self,event):
        page=self.futurContenu(self.base, self.root, self.token, self,self.id,True)

    def supprimer(self,event):
        if self.type == "Classe":
            self.service.supprimerClasse(self.id)
        elif self.type == "Page" :
            self.service.supprimerPage(self.id)
        elif self.type == "Exercice" :
            self.service.supprimerExercice(self.id)
        
        self.parent.MainPage()

    def managementPage(self):
        self.init()
        frame = Frame(self.root)
        BouttonAjoutExercice = Button(frame,text="Ajouter des exercices")
        BouttonAjoutExercice.pack(side=LEFT)
        # BouttonOrdre=Button(frame, text="Modifier l'ordre des exercices")
        # BouttonOrdre.pack(side=LEFT)
        BouttonFuturContenu=Button(frame, text="Gestion des exercices")
        BouttonFuturContenu.pack(side=LEFT)
        BouttonSupression=Button(frame, text="Supprimer la page")
        BouttonSupression.pack(side=RIGHT)
        frame.place(x=(self.root.winfo_width()/2) - 200, y=(self.root.winfo_height()/2) -  100)

        BouttonAjoutExercice.bind("<1>", self.addExercices)
        # BouttonOrdre.bind("<1>", self.modifierOrdreExercices)
        BouttonFuturContenu.bind("<1>", self.sectionFuturContenu)
        BouttonSupression.bind("<1>", self.supprimer)

    def addExercices(self,event):
        fenetre = Toplevel() 
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        self.listeExercices(frame)
        BouttonAjout=Button(fenetre, text="Ajouter des exercices",bg="cyan",width=30,height=3)
        BouttonAjout.pack(side=LEFT)
        BouttonRetirer=Button(fenetre, text="Retirer des exercices",bg="cyan",width=30,height=3)
        BouttonRetirer.pack(side=LEFT)
        BouttonQuit=Button(fenetre, text="Fermer",bg="cyan",width=30,height=3)
        BouttonQuit.pack(side=RIGHT)
        frameInfo = Frame(fenetre)
        frameInfo.pack(side=BOTTOM)
        labelInfo = Label(frameInfo,text="")
        self.info = InfoBulle(frameInfo,labelInfo)
        BouttonQuit.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        BouttonAjout.bind("<1>",lambda event, fr=frame : self.ajouterExercices(event,fr))
        BouttonRetirer.bind("<1>",lambda event, fr=frame : self.retirerExercices(event,fr))
        fenetre.mainloop()

    def listeExercices(self,frame):
        self.clean(frame)
        exercicesAjoutes = self.service.getExercices(self.id)
        label = Label(frame,text="Liste des exercices de la page :" + self.nom,font=("Arial",30))
        label.pack(side=TOP)
        if exercicesAjoutes != None and len(exercicesAjoutes)> 0 :
            for key,exercice in exercicesAjoutes.items():
                frameExercice = Frame(frame)
                frameExercice.pack(side=TOP)
                nomExercice = Label(frameExercice,text="Exercice : " + exercice["nom"] + "   ",font=("Arial",20))
                nomExercice.pack(side=LEFT)
        else : 
            label_listeExercicesEmpty = Label(frame,text="Aucun Exercice....",font=("Arial",30))
            label_listeExercicesEmpty.pack(side=TOP)

    
    def ajouterExercices(self,event, fr):
        fenetre = Toplevel() 
        label = Label(fenetre,text="Liste des exercices à ajouter à la page :" + self.nom,font=("Arial",30))
        label.pack(side=TOP)
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        self.listeAddExos(frame)
        BouttonQuit=Button(fenetre, text="Fermer",bg="cyan",width=30,height=3)
        BouttonQuit.pack(side=BOTTOM)
        BouttonQuit.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        fenetre.mainloop()
        self.listeExercices(fr)





    def listeAddExos(self, frame):
        self.clean(frame)
        serviceExos = Exercices_services(self.token)
        Exercices = serviceExos.getExercices()
        exercicesAjoutes = self.service.getExercices(self.id)
        if Exercices != None and len(Exercices)> 0 and len(Exercices) > len(exercicesAjoutes)  :
            for key,exercice in Exercices.items():
                frameExercice = Frame(frame)
                frameExercice.pack(side=TOP)
                if len(exercicesAjoutes) > 0 :
                    b = False
                    for ke,ex in exercicesAjoutes.items():
                        if ex["idExercice"] == exercice["idExercice"]:
                            b = True
                    if not b:
                        nomExercice = Label(frameExercice,text="Exercice : " + exercice["nom"] + "   ",font=("Arial",20))
                        nomExercice.pack(side=LEFT)
                        BouttonAjout=Button(frameExercice, text="Ajouter",bg="cyan")
                        BouttonAjout.pack(side=RIGHT)
                        BouttonAjout.bind("<1>",lambda event, idExercice=exercice["idExercice"],nomExercice=exercice["nom"] : self.ajoutExo(event,frame,idExercice,nomExercice))
                else :
                    nomExercice = Label(frameExercice,text="Exercice : " + exercice["nom"] + "   ",font=("Arial",20))
                    nomExercice.pack(side=LEFT)
                    BouttonAjout=Button(frameExercice, text="Ajouter",bg="cyan")
                    BouttonAjout.pack(side=RIGHT)
                    BouttonAjout.bind("<1>",lambda event, idExercice=exercice["idExercice"],nomExercice=exercice["nom"] : self.ajoutExo(event,frame,idExercice,nomExercice))
        else : 
            label_listeExercicesEmpty = Label(frame,text="Aucun Exercice....",font=("Arial",30))
            label_listeExercicesEmpty.pack(side=TOP)

    def ajoutExo(self,event,frame,idExo,nomExo):
        self.clean(frame)
        message = self.service.addExercice(self.id,idExo,nomExo)
        self.info.affiche(message)
        self.listeAddExos(frame)

    def retirerExercices(self, event, fr):
        exercicesAjoutes = self.service.getExercices(self.id)
        fenetre = Toplevel() 
        label = Label(fenetre,text="Liste des exercices à retirer de la page :" + self.nom,font=("Arial",30))
        label.pack(side=TOP)
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        self.listeRemoveExos(frame)
        BouttonQuit=Button(fenetre, text="Fermer",bg="cyan",width=30,height=3)
        BouttonQuit.pack(side=BOTTOM)
        BouttonQuit.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        fenetre.mainloop()
        self.listeExercices(fr)

    def listeRemoveExos(self, frame):
        Exos = self.service.getExercices(self.id)
        if Exos != None and len(Exos)> 0 :
            for key,exercice in Exos.items():
                frameExercice = Frame(frame)
                frameExercice.pack(side=TOP)
                nomExercice = Label(frameExercice,text="Exercice : " + exercice["nom"] + "   ",font=("Arial",20))
                nomExercice.pack(side=LEFT)
                if len(Exos) > 0 :
                    BouttonAjout=Button(frameExercice, text="Retirer",bg="cyan")
                    BouttonAjout.pack(side=RIGHT)
                    BouttonAjout.bind("<1>",lambda event, idExercice=exercice["idExercice"],nomExercice=exercice["nom"] : self.removeExo(event,frame,idExercice,nomExercice))
                # script = None
                # if "script" in exercice :
                #     script = exercice["script"]
        else : 
            label_listeExercicesEmpty = Label(frame,text="Aucun Exercice....",font=("Arial",30))
            label_listeExercicesEmpty.pack(side=TOP)

    def removeExo(self,event,frame,idExo,nomExo):
        self.clean(frame)
        message = self.service.removeExercice(self.id,idExo,nomExo)
        self.info.affiche(message)
        self.listeRemoveExos(frame)
        
    def clean(self,fen):
        for child in fen.winfo_children():
            child.destroy()

    def modifierOrdreExercices(self,event):
        pass

    def managementExercice(self):
        self.init()
        frame = Frame(self.root)
        BouttonEditerExercice = Button(frame,text="Éditer l'exercice")
        BouttonEditerExercice.pack(side=LEFT)
        # BouttonTestExercice=Button(frame, text="Tester l'exercice")
        # BouttonTestExercice.pack(side=LEFT)
        BouttonDifficulte=Button(frame, text="Modifier le niveau de difficulté de l'exercice")
        BouttonDifficulte.pack(side=LEFT)
        BouttonSupression=Button(frame, text="Supprimer l'exercice'")
        BouttonSupression.pack(side=RIGHT)
        frame.place(x=(self.root.winfo_width()/2) - 200, y=(self.root.winfo_height()/2) -  100)

        BouttonSupression.bind("<1>", self.supprimer)
        BouttonEditerExercice.bind("<1>", self.editerExercice)
        # BouttonTestExercice.bind("<1>", self.testerExercice)
        BouttonDifficulte.bind("<1>", self.changerDifficulte)

    def editerExercice(self,event):
        editor = Editor(self.root,self.token,self,self.id,self.nom,True)

    # def testerExercice(self,event):
    #     pass
        

    def changerDifficulte(self,event):
        fenetre = Toplevel()
        # difficulte
        label = Label(fenetre,text="Entrez la nouvelle difficulté de l'exercice : ")
        label.pack(side=TOP)
        cbbDificulte = ttk.Combobox(fenetre, values=["Facile","Moyen","Difficile"])
        cbbDificulte.current(0)
        cbbDificulte.pack(side=TOP)
        frame = Frame(fenetre)
        frame.pack(side=TOP)
        btn_valider = Button(frame,text="Valider")
        btn_valider.pack(side=RIGHT)
        btn_annuler= Button(frame,text="Annuler")
        btn_annuler.pack(side=LEFT)

        btn_valider.bind("<1>",lambda event, fen=fenetre,difficulte=cbbDificulte : self.modifierDifficulteExercice(event,fen,difficulte))
        btn_annuler.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))

        fenetre.mainloop()
        

    def modifierDifficulteExercice(self,event,fen,difficulte):
        message = self.service.modifierDifficulteExercice(self.id,difficulte.get())
        self.Quitter(event,fen)


    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()