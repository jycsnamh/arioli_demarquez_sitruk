import firebase_admin
from firebase_admin import credentials
from tkinter import *
from tkinter import ttk
from tkintertable import *
from credentials.CodeAccess import CodeAccess
from credentials.Login import *
from services.Utilisateurs_services import *

# def inscription(event, us):
#     insc = Inscription(us)

# def connexion(event, us, token):
#     connex = Connexion(us)
#     token = connex.getToken()
    
class Main:
    def __init__(self):
        self.base = self
        self.root = Tk()
        self.root.title("Classy")
        self.root.state('zoomed')
        self.root.update()
        self.main()
        self.root.mainloop()

    def main(self,event=None):
        self.clean()
        title = Label(self.root,text="Classy",font=("Arial",45))
        title.place(x=400,y=200)
        subtitle = Label(self.root,text="L'éditeur d'exercices - Partie Prof",font=("Arial",20,"italic"))
        subtitle.place(x=400,y=290)
        style = ttk.Style()
        style.configure("W.TButton", background="orange")
        btnQuit = Button(self.root,text="Quitter",width=30,style="W.TButton")
        loginUser = Login(self.base,self.root)
        #accessEleve = CodeAccess(self.base,self.root)

        btnQuit.place(x=self.root.winfo_width()/2 - 150,y=self.root.winfo_height()-70,height=50)
        btnQuit.bind("<1>",self.Quitter)

    def Quitter(self,event = None):
        self.root.quit()
        self.root.destroy()

    def clean(self):
        for child in self.root.winfo_children():
            child.destroy()

if __name__ == '__main__':
    main = Main()
    
    