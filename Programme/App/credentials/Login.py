from tkinter import *
from tkinter import ttk
from tkinter import font
import tkinter
from tkintertable import *
from credentials.Connexion import Connexion

from credentials.Inscription import Inscription


class Login():


    def __init__(self,base,root):
        self.root = root
        self.base = base
        
        btn_inscription = Button(self.root, text="Inscription",width=20)
        btn_inscription.place(x= 400,y=self.root.winfo_height()/2 + 100,height=50)
        btn_inscription.bind("<1>", lambda event : Inscription(event,self.base, self.root))
        btn_connexion = Button(self.root, text="Connexion",width=20)
        btn_connexion.place(x=400,y=self.root.winfo_height()/2,height=50)
        btn_connexion.bind("<1>", lambda event : Connexion(event,self.base, self.root))

     
    
        