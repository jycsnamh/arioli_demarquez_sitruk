from tkinter import *
from dashboard.eleve.DashBoardEleve import DashBoardEleve
from utils.InfoBulle import InfoBulle
from services.Eleves_services import *

class CodeAccess:
    def __init__(self,base,root):
        self.service = Eleves_services()
        self.root = root
        self.base = base
        frame = Frame(self.root)
        frame.place(x=self.root.winfo_width()/5,y=self.root.winfo_height()/2)
        self.label_info = Label(frame,text="",foreground="red")
        self.label_info.pack(side=TOP)
        self.info = InfoBulle(self.root,self.label_info)
        framePrenom = Frame(frame)
        framePrenom.pack(anchor=NW)
        frameCode = Frame(frame)
        frameCode.pack(anchor=NW)
        infoFieldPrenom = Label(framePrenom,text="Saisir votre prénom :", font=("Arial",16))
        infoFieldPrenom.pack(anchor=NW)
        valuePrenom = StringVar()
        valuePrenom.set("")
        prenomEntry = Entry(framePrenom, textvariable=valuePrenom, width=50)
        prenomEntry.pack(anchor=NW)
        infoFieldCode = Label(frameCode,text="Saisir le code d'accès pour entrer dans la classe :", font=("Arial",16))
        infoFieldCode.pack(anchor=NW)
        valueCode = StringVar()
        valueCode.set("")
        codeEntry = Entry(frameCode, textvariable=valueCode, width=50)
        codeEntry.pack(anchor=NW)
        btn_valider = Button(frame,text="Valider")
        btn_valider.pack(anchor=SE)

        btn_valider.bind("<1>",lambda event, vp=valuePrenom, pe=prenomEntry, vc=valueCode, ce=codeEntry : self.checkConnexion(event, vp, pe, vc, ce))

    def checkConnexion(self, event, vp, pe, vc, ce):
        login = {}
        login = self.service.login(pe.get(),ce.get())
            
        if "token" in login :
            dashboardEleve = DashBoardEleve(self.base,self.root,login["token"],self.service)
        else:
            self.info.affiche(login["message"])