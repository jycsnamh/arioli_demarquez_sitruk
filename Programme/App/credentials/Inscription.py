from tkinter import *
from tkinter import ttk
from tkinter import font
import tkinter
from tkintertable import *
from utils.InfoBulle import InfoBulle
from credentials.Connexion import Connexion

from services.Utilisateurs_services import Utilisateurs_services

class Inscription():
    def __init__(self,event,base,root):
        self.base = base
        self.root = root
        self.event = event

        self.base.clean()

        frame = Frame(self.root)
        label = Label(frame, text="Inscription" , font=("Arial",16,"bold"))
        label.pack(side=TOP)
        
        
        label = Label(frame, text= "Pseudo:")
        label.pack(side=TOP)

        
        self.pseudoEntre = Entry(frame, width=15)
        self.pseudoEntre.pack(side=TOP)

       
        label = Label(frame, text= "Mot de passe:")
        label.pack(side=TOP)

        
        self.mdpEntre = Entry(frame, width=15, show="*")
        self.mdpEntre.pack(side=TOP)

        
        label = Label(frame, text= "Nom:")
        label.pack(side=TOP)

        
        self.nomEntre = Entry(frame, width=15)
        self.nomEntre.pack(side=TOP)

       
        label = Label(frame, text= "Prenom:")
        label.pack(side=TOP)

        
        self.prenomEntre = Entry(frame, width=15)
        self.prenomEntre.pack(side=TOP)
        
        frameBtn =Frame(frame)
        frameBtn.pack(side=TOP)
        btn_annuler = Button(frameBtn, text="Annuler")
        btn_annuler.pack(side=LEFT)
        btn_valider = Button(frameBtn, text="Valider")
        btn_valider.pack(side=RIGHT)

        labelInfo = Label(frame, text="", foreground="red")
        labelInfo.pack(side=TOP)
        self.info = InfoBulle(frame,labelInfo)
    
        btn_annuler.bind("<1>", self.base.main)
        btn_valider.bind("<1>", lambda event : self.Valider(event))
        self.mdpEntre.bind("<Return>", lambda event : self.Valider(event))
        self.pseudoEntre.bind("<Return>", lambda event : self.Valider(event))
        self.prenomEntre.bind("<Return>", lambda event : self.Valider(event))
        self.nomEntre.bind("<Return>", lambda event : self.Valider(event))

        frame.place(x=(self.root.winfo_width()/2) - 70, y=(self.root.winfo_height()/2) -  100)

    def Valider(self,event):
        user = {"pseudo": self.pseudoEntre.get(), "password": self.mdpEntre.get(), "nom": self.nomEntre.get(), "prenom" : self.prenomEntre.get()}
        us = Utilisateurs_services()
        if  self.pseudoEntre.get() != "" and self.mdpEntre.get() != "" and self.nomEntre.get() != "" and self.prenomEntre.get() != "" :
            us.register(user)
            Connexion(event,self.base,self.root)
        else :
            self.info.affiche("Veuillez remplir tous les champs.")
        
        
        
