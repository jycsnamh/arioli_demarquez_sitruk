from tkinter import *
from tkinter import ttk
from tkinter import font
from tkintertable import *

from utils.InfoBulle import InfoBulle
from dashboard.prof.Dashboard import Dashboard
from services.Utilisateurs_services import Utilisateurs_services

class Connexion():

    def __init__(self,event,base,root):
        self.root = root
        self.base = base
        self.event = event
        self.service = Utilisateurs_services()
        self.co()
        
    def co(self,event=None):
        self.base.clean()

        frame = Frame(self.root)
        
        self.label_info = Label(frame,text="",foreground="red")
        self.label_info.pack(side=TOP)
        self.info = InfoBulle(self.root,self.label_info)

        label = Label(frame, text="Connexion" , font=("Arial",16,"bold"))
        label.pack(side=TOP)
        
        
        label = Label(frame, text= "Pseudo:")
        label.pack(side=TOP)

        
        self.pseudoEntre = Entry(frame, width=15)
        self.pseudoEntre.pack(side=TOP)

       
        label = Label(frame, text= "Mot de passe:")
        label.pack(side=TOP)

        
        self.mdpEntre = Entry(frame, width=15, show="*")
        self.mdpEntre.pack(side=TOP)

        
        frameBtn =Frame(frame)
        frameBtn.pack(side=TOP)
        btn_annuler = Button(frameBtn, text="Annuler")
        btn_annuler.pack(side=LEFT)
        btn_valider = Button(frameBtn, text="Se connecter")
        btn_valider.pack(side=RIGHT)


        btn_annuler.bind("<1>", self.base.main)
        btn_valider.bind("<1>", lambda event : self.Valider(event))
        self.mdpEntre.bind("<Return>",lambda event, : self.Valider(event))
        self.pseudoEntre.bind("<Return>",lambda event, : self.Valider(event))

        frame.place(x=(self.root.winfo_width()/2) - 50, y=(self.root.winfo_height()/2) -  100)

    def Valider(self,event):
        self.user = {"pseudo": self.pseudoEntre.get(), "password": self.mdpEntre.get()}
        self.token = self.service.login(self.user.get("pseudo"),self.user.get("password"))
        
        
        if self.token != None:
            self.dashboard=Dashboard(self.base, self.root, self.token,self)
        else:
            self.info.affiche("Informations incorrectes.")



