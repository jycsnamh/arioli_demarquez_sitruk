import types

class Globs:
    def __init__(self):
        pass
    def createGlobs(self,name,value):
        globals()[name] = value

    def getValue(self,s):
        if s in globals():# and isinstance(globals()[s], types.ClassType):
            return globals()[s]
        return None

    def dictToList(self,d,field=None):
        tab = []
        nbL = 0
        nbC = 0
        b = False
        for key,value in d.items():
            l = []
            for k,v in value.items():
                if field != None:
                    if k == field:
                        l.append([k,v])
                        if not b:
                            nbC = nbC + 1
                else:
                    l.append([k,v])
                    if not b:
                        nbC = nbC + 1
            b = True            
            tab.append(l)
            nbL = nbL + 1
        return tab,nbL,nbC

    def listToDict(self,lk,lv):
        d = {}
        for i in range(len(lk)):
            d[lk[i]] = lv[i]
        return d
