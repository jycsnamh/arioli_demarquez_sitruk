
from tkinter import *


class ScrollableFrame(Frame):
    def __init__(self, container,side,fill=None,expand=None, *args, **kwargs):
        super().__init__(container, *args, **kwargs)
        self.fill = fill
        self.expand =expand
        self.side = side
        self.canvas = Canvas(container,*args, **kwargs)
        self.scrollbar = Scrollbar(container, orient="vertical", command=self.canvas.yview)
        self.scrollable_frame = Frame(self.canvas)
        self.scrollable_frame.pack()
        self.scrollable_frame.bind("<Configure>",lambda e: self.canvas.configure(scrollregion=self.canvas.bbox("all")))
        self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)

        self.canvas.create_window((0, 0), window=self.scrollable_frame, anchor="nw")

        self.canvas.configure(yscrollcommand=self.scrollbar.set)

        
        self.scrollbar.pack(side="right", fill="y")
        
        self.canvas.create_rectangle

    def _on_mousewheel(self, event):
        self.canvas.yview_scroll(int(-1*(event.delta/120)), "units")

    def getFrame(self):
        return self.scrollable_frame

    def show(self):
        
       
        if self.expand != None :
            self.canvas.pack(fill=self.fill,expand=self.expand)
        else :
            self.canvas.pack(side=self.side)