from os import path
from pathlib import Path
from tkinter import *

import PIL.Image, PIL.ImageTk
from editor.menus.MenuComponent import *

class DragManager():

    

    def __init__(self,root,widget,sess,container,globs,drag=True,drop=True,bContainer = False,edit=True,toolBox = False,copy=False):
        
        self.root = root
        
        self.sess = sess

        self.widget = widget

        self.container = container

        self.bContainer = bContainer

        self.globs = globs

        self.DM = self.__class__

        self.drag = drag

        self.drop = drop

        self.edit = edit

        self.tb = toolBox
        
        self.copy = copy

        if self.copy:
            self.clone = None


        
    def start(self):
        if self.drag:
            self.add_dragable()

        if self.drop:
            self.globs.getValue("listeDrop").append(self.widget)
        


    def add_dragable(self):
        # if self.edit and not self.tb :
        #     xd = self.widget.winfo_rootx()

        #     yd = self.widget.winfo_rooty()

        #     self.widget.place(x=xd,y=yd)
            
        #     if self.container != None :
        #         self.sess.update(self.container)
        #     else:
        #         self.sess.update(self.widget)

        if self.copy:
            self.widget.bind("<ButtonPress-1>", self.on_start)

        if self.edit:
            self.widget.bind("<ButtonPress-3>", self.param)

        
        self.widget.bind("<B1-Motion>", self.on_drag)
        
        self.widget.bind("<ButtonRelease-1>", self.on_drop)

        self.widget["cursor"] = "hand2"

    def on_start(self, event):
        if not self.edit and self.tb:
            self.clone = self.widget.clone(self.root)
            self.clone.place(x = self.widget.winfo_rootx(),y = self.widget.winfo_rooty())

    def on_drag(self, event):
        
        if not self.edit and self.copy:
            xd = self.widget.winfo_rootx() + event.x - self.clone.winfo_width() -1 #+ self.widget.winfo_width()/2
            yd = self.widget.winfo_rooty() + event.y - self.clone.winfo_height() -1 #+ self.widget.winfo_height()/2
            self.clone.place(x=xd,y=yd)
        else :
            xd = self.widget.winfo_rootx() + event.x - self.widget.winfo_width() -1 #+ self.widget.winfo_width()/2
            yd = self.widget.winfo_rooty() + event.y - self.widget.winfo_height() -1 #+ self.widget.winfo_height()/2
            if self.container != None :
                self.container.place(x=xd,y=yd)
                self.container.x = xd
                self.container.y = yd
                self.sess.update(self.container)
            else:
                self.widget.place(x=xd,y=yd)
                self.widget.x = xd
                self.widget.y = yd
                self.sess.update(self.widget)

    def on_drop(self, event):

        # commencons par trouver le widget sous le curseur de la souris

        x,y = event.widget.winfo_pointerxy()

        target = event.widget.winfo_containing(x,y)
        
        if target in self.globs.getValue("listeDrop"):
            
            if str(target.__class__).find("Texte") != -1 :
                target.config(state=NORMAL)
                target.insert(1.0,event.widget.get(1.0,END))
                target.config(state=DISABLED)
            elif str(target.__class__).find("Img") != -1 :
                img = PIL.Image.open(self.widget.cget("text"))
                
                pho = PIL.ImageTk.PhotoImage(img)
                
                target.config(image=pho)
                target.image = pho
                target["width"] = 220
                target["height"] = 220
                target.config(text=self.widget.cget("text"))
                target.setValue(self.widget.getValue())
            elif str(target.__class__).find("Son") != -1 :
                print(self.widget.fichier)
                print("teesssttt")
                target.init(self.widget.fichier)
                target.getSound().click()
                target["text"] = self.widget["text"]
                target.setValue(self.widget.getValue())
        if not self.edit and self.copy :
            self.clone.destroy()
            

    def param(self,event):
        if self.bContainer :
            print("test2")
            self.menu = MenuComponent(self.root,self.container,event,self.sess,self.globs,self.DM,self.container)
        else :
            if str(self.widget.__class__).find("Container") != -1 :
                
                self.menu = MenuComponent(self.root,self.widget,event,self.sess,self.globs,self.DM)
            else:
                self.menu = MenuComponent(self.root,self.widget,event,self.sess,self.globs)

    def stop(self):
        self.widget.unbind("<ButtonPress-1>")

        self.widget.unbind("<ButtonPress-3>")

        
        self.widget.unbind("<B1-Motion>")
        
        self.widget.unbind("<ButtonRelease-1>")
    
    