import json
from tkinter import *

from editor.components.items.Bouton import Bouton

class Table: 
      
    def __init__(self,root,tab,nbRow,nbCol,listeChamps,globs): 
        self.globs = globs        

        for j in range(len(listeChamps)):
            self.e = Label(root,text=listeChamps[j], width=20, fg='black', 
                               font=('Arial',12,'bold')) 
            self.e.grid(row=0, column=j) 

        for i in range(1,nbRow+1): 
            k=0
            for j in range(nbCol): 
                if tab[i-1][j][0] == "nom":
                    self.e = Bouton("Button",root,None,text=tab[i-1][j][1])
                    lk =[]
                    lv = []
                    for k in range(nbCol):
                        lk.append(tab[i-1][k][0])
                        lv.append(tab[i-1][k][1])
                    self.e.setValue(self.globs.listToDict(lk,lv))
                    self.e.grid(row=i, column=0) 
                    self.e.bind("<1>", self.select)
                elif tab[i-1][j][0] == "difficulte":
                    self.e = Label(root, text=tab[i-1][j][1], width=20, fg='blue', font=('Arial',12)) 
                    self.e.grid(row=i, column=1) 
                  
    def select(self,event):
        self.globs.createGlobs("selected",event.widget.getValue())
        print(self.globs.getValue("selected"))
