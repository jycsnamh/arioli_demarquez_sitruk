from tkinter import *

class InfoBulle :
    def __init__(self,root,label):
        self.root = root
        self.info = label
        #

    def affiche(self,message):
        self.info.config(text=message)
        self.root.after(1000,self.efface)

    def efface(self):
        self.info.config(text="")