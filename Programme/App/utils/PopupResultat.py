from tkinter import *
from services.Eleves_services import *

class PopupResultat :
    def __init__(self,result):
        self.printResult(result)

    def printResult(self,result):
        popup = Toplevel()
        if result :
            labelResult = Label(popup,text="BRAVO !",font=("Arial",40,"bold"),fg="green")
            labelResult.pack(side=TOP)
        else :
            labelResult = Label(popup,text="DOMMAGE...",font=("Arial",40,"bold"),fg="red")
            labelResult.pack(side=TOP)
        btn_quit = Button(popup,text="Fermer")
        btn_quit.pack(side=BOTTOM)
        btn_quit.bind("<1>",lambda event, fen=popup : self.Quitter(event,fen))
        popup.mainloop()

    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()