import pygame

class Sound:
    def __init__(self,boutton):
        self.url = None
        self.bplay = False
        self.boutton = boutton
        pygame.mixer.init(44100, -16,2,2048)

    def load(self,url):
        self.url = url

    def click(self,event=None):
        if not self.bplay :
            self.play()
            self.bplay = True
        else:
            self.pause()
            self.bplay = False

    def play(self):
        pygame.mixer.music.load(self.url)
        pygame.mixer.music.play()
        self.bplay = False

    def pause(self):
        pygame.mixer.music.pause()