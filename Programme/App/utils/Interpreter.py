from pathlib import Path
import pathlib
from tkinter import *
from tkinter import ttk
import PIL.Image, PIL.ImageTk
import tkinter.font as font
#import image_slicer
import types
import sys
import re
from utils.ScrollableFrame import ScrollableFrame
from utils.DragManager import DragManager   
from utils.PopupResultat import *

# types composants
from editor.components.items.Son import *
from editor.components.containers.ContainerComponent import *
from editor.components.items.Forme import *

class Interpreter :
    def __init__(self,globs):
        self.globs = globs
        self.fe = ""
        self.script = ""
    
    def load(self,root,sess,edit = False,w = "",side = RIGHT):
        widgets =  sess.getScript().split(" ; ")[1].split(";$")
        widgets.pop(len(widgets)-1)
        for widgetScript in widgets :
            if w == "Exec" or w == "" :
                if widgetScript.find("ToolBox") != -1:
                    partsWidgetScript = widgetScript.split(";!")
                    self.parse(root,sess,partsWidgetScript[2],partsWidgetScript[0].split("=")[1],None,partsWidgetScript[3].split("::")[1],edit,side,w)
                else:
                    partsWidgetScript = widgetScript.split(";!")
                    self.parse(root,sess,partsWidgetScript[2],partsWidgetScript[0].split("=")[1],partsWidgetScript[3].split("=")[1],partsWidgetScript[4].split(";,"),edit,side,w)
            else:
                if widgetScript.find("ToolBox") != -1 and w == "ToolBox":
                    partsWidgetScript = widgetScript.split(";!")
                    self.parse(root,sess,partsWidgetScript[2],partsWidgetScript[0].split("=")[1],None,partsWidgetScript[3].split("::")[1],edit,side,w)
                else:
                    partsWidgetScript = widgetScript.split(";!")
                    self.parse(root,sess,partsWidgetScript[2],partsWidgetScript[0].split("=")[1],partsWidgetScript[3].split("=")[1],partsWidgetScript[4].split(";,"),edit,side,w)
        root.update()

    def parse(self,root,sess,className,idWidget,Type,listAttr,edit,side,w):
        cls = self.globs.getValue(className)
        widget = None
        if className.find("Container") != -1:
            widget = cls(Type,root,self.globs.getValue(listAttr[len(listAttr)-2].split("=")[1]),sess)
        elif className.find("ToolBox") != -1 :
            toolBox = ScrollableFrame(root,side=RIGHT,height=800)
            if w == "Exec" or w == "ToolBox":
                toolBox.show()
            widget = cls(toolBox.getFrame(),sess,self.globs)
        else :
            widget = cls(Type,root,self.globs.getValue(listAttr[len(listAttr)-2].split("=")[1]))         
        widget.id = idWidget
        if className.find("ToolBox") != -1 :
            children = [listAttr]
            if children[0].find(";§") != -1:
                children = listAttr.split(";§")
            
            for ch in children:
                if ch.find(";/") != -1 :
                    c = ch.split(";/")
                    cls = self.globs.getValue(c[1])
                    child = cls(c[2],widget.getToolBox(),self.globs.getValue(c[3].split(";?")[len(c[3].split(";?"))-2].split("=")[1]),width=150,height=150)
                    child.id = c[0].split("=")[1]
                    lAttr = c[3].split(";?")
                    img = None
                    for attr in lAttr:
                        att = attr.split("=")
                        if att[0] != "class" and att[0] != "colormap" and att[0] != "visual":
                            if att[0] != "menu":
                                if att[0] != "x" and att[0] != "y"  :
                                    if att[0] == "value":
                                        child.setValue(att[1])
                                    elif att[0] == "fichier":
                                        if c[1].find("Son") != -1 :
                                            child.init(att[1])
                                            child.bind("<1>",child.getSound().click)
                                    elif att[0] == "resultatAttendu" :
                                        child.setResultatAttendu(att[1])
                                    elif att[0] == "text" and c[1].find("Texte") != -1 :
                                        child.set(att[1])
                                    elif att[0] == "text" and c[1].find("Img") != -1 and att[1] != "" :
                                        img = PIL.Image.open(open(att[1],'rb'))
                                        child.config({att[0] : att[1]})
                                    elif att[0].find("add::") == -1 and att[0].find("container") == -1 and att[0] != "image":
                                        if re.search("[a-zA-Z]", att[1]) or att[1] =="" :    
                                            child.configure({att[0]: att[1]})
                                        else :
                                            child.configure({att[0]: int(att[1])})
                                else : 
                                    if att[0] == "x" :
                                        child.x = int(att[1])
                                    else:
                                        child.y = int(att[1])
                                    # xy.append(int(att[1]))
                    if c[1].find("Img") != -1 and img != None:
                        #imgrsz = img.resize((child["width"],child["height"]),PIL.Image.ANTIALIAS)
                        pho = PIL.ImageTk.PhotoImage(img)
                        child.config(image= pho)
                        child.image = pho
                    if w != "ToolBox":
                        child.dm = DragManager(root,child,sess,None,self.globs,True,False,False,edit,True,True)
                    else :
                        child.dm = DragManager(root,child,sess,None,self.globs,True,False,False,edit,True)
                    child.dm.start()
                    widget.add(child)    
                    child.pack(side=TOP)
            if not edit :
                widget.getToolBox().pack(side=side)
            else:
                widget.formatEdit(root).pack(anchor=NE)
        else:
            container = None
            bContainer = False
            img = None
            xy = []
            for attr in listAttr:
                att = attr.split("=")
                if att[0] != "class" and att[0] != "colormap" and att[0] != "visual":
                    if att[0] != "menu":
                        if att[0] != "x" and att[0] != "y"  :
                            if att[0] == "value":
                                if className.find("Son") != -1 :
                                    widget.setValue(att[1])
                                else:
                                    widget.setValue(att[1])
                            elif att[0] == "fichier":
                                if className.find("Son") != -1 :
                                    widget.init(att[1])
                                    widget.bind("<1>",widget.getSound().click)
                            elif att[0] == "actions":
                                widget.setActions(att[1])
                                self.makeAction(widget,root)
                            elif att[0] == "resultatAttendu" :
                                widget.setResultatAttendu(att[1])
                            elif att[0] == "text" and className.find("Texte") != -1 :
                                widget.set(att[1])
                            elif att[0] == "text" and className.find("Img") != -1 and att[1] != "":
                                img = PIL.Image.open(open(att[1],'rb'))
                                widget.config({att[0] : att[1]})
                            elif att[0] == "active" :
                                if att[1] == "NORMAL":
                                    widget.active = "NORMAL"
                                    if not edit:
                                        widget.config(state=NORMAL)
                                else:
                                    widget.active = "DISABLED"
                                    widget.config(state=DISABLED)
                            elif att[0].find("add::") == -1 and att[0].find("container") == -1 and att[0] != "image":
                                if (re.search("[a-zA-Z]", att[1]) or att[1] =="")  :    
                                    widget.configure({att[0]: att[1]})
                                else :
                                    widget.configure({att[0]: int(att[1])})
                            elif att[0].find("container") != -1 :
                                elemContainer = att[1].split("::")
                                if elemContainer[0].find("Img") != -1:  
                                    widget.changeToContainerImage(DragManager,self.globs,edit)
                                elif elemContainer[0].find("Texte") != -1:
                                    widget.changeToContainerText(DragManager,self.globs,edit)
                                elif elemContainer[0].find("Son") != -1:
                                    widget.changeToContainerSon(DragManager,self.globs,edit)
                                if widget.getChild() != None:
                                    bContainer = True
                            else : 
                                pass
                        else : 
                            if att[0] == "x" :
                                widget.x = int(att[1])
                            else:
                                widget.y = int(att[1])
                            # xy.append(int(att[1]))
            if className.find("Img") != -1 and img != None:
                # imgrsz = img.resize((widget["width"],widget["height"]),PIL.Image.ANTIALIAS)
                pho = PIL.ImageTk.PhotoImage(img)
                widget.config(image= pho)
                widget.image = pho
            if edit and not bContainer :
                widget.dm = DragManager(root,widget,sess,None,self.globs,True,False)
                widget.dm.start()
            elif className.find("Container") != -1:
                print("test1")
                widget.dm = DragManager(root,widget,sess,None,self.globs,False,True)
                widget.dm.start()
            widget.place(x=widget.x,y=widget.y)

    def makeAction(self,widget,root):
        actions = widget.getActions().split("//")
        if actions[0].find("0") != -1:
            val = actions[1]
            widget.bind("<1>",lambda event, val=val :self.help(event,val))
        elif actions[0].find("1") != -1:
            widget.bind("<1>",lambda event, root = root : self.results(event,root))
        elif actions[0].find("2") != -1:
            widget.bind("<1>",lambda event, root = root : self.cleanAllContainers(event,root))
            
    def results(self,event,root):
        containers = self.listeContainers(root)
        textes = self.listeTextes(root)
        res = 0
        for cont in containers:
            if str(cont.getChild().__class__).find("Texte") != -1 :
                if cont.getResultatAttendu().strip() == cont.getChild().get(1.0,END).strip():
                    res = res + 1
            else:
                if cont.getResultatAttendu() == cont.getChild().getValue():
                    res = res + 1
        for t in textes:
            if t.get(1.0,END).strip() == t.getResultatAttendu().strip():
                res = res + 1
        val = res == len(containers) + len(textes)
        afficheRes = PopupResultat(val)

    def cleanAllContainers(self,event,root):
        containers = self.listeContainers(root)
        for cont in containers :
            cont.reinitContainer(DragManager,self.globs)

    def listeTextes(self,root):
        textes = []
        for child in root.winfo_children():
                if str(child.__class__).find("Texte") != -1 and child["state"] == NORMAL:
                    textes.append(child)
        return textes

    def listeContainers(self,root):
        containers = []
        for child in root.winfo_children():
                if str(child.__class__).find("Container") != -1 :
                    containers.append(child)
        return containers

    def help(self,event,val):
        fenetre = Toplevel()
        labelAide = Label(fenetre,text="Aide")
        labelAide.pack(side=TOP)
        aide = Text(fenetre)
        aide.insert(1.0,val)
        aide.config(state = DISABLED)
        aide.pack(side=TOP)
        btnQuit = Button(fenetre,text="Fermer")
        btnQuit.pack(side=TOP)
        btnQuit.bind("<1>",lambda event, fen=fenetre : self.Quitter(event,fen))
        fenetre.mainloop()

    def Quitter(self,event,fen):
        fen.quit()
        fen.destroy()