from pathlib import Path
import pathlib
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import PIL.Image, PIL.ImageTk
from editor.components.items.Img import *
from editor.components.items.Son import *
from utils.ScrollableFrame import *
from shutil import copy
import os

class Bibliotheque:
    path = Path.cwd()
    def __init__(self,Type, widget,sess,container = None,edit =True) :
        self.type = Type
        self.container = container
        self.widget = widget
        self.sess = sess
        self.fenetre = Toplevel()
        #self.fenetre.state('zoomed')
        self.fenetre.geometry ( "1200x900" )

        volet = Frame(self.fenetre)
        volet.pack(side=TOP)

        addNew = ""
        if edit :
            if self.type == "Image":
                addNew = "Ajouter une nouvelle image"
            elif self.type == "Son":
                addNew = "Ajouter un nouveau son"
            btn_addNewItem = Button(volet,text=addNew)
            btn_addNewItem.pack(side=LEFT)

        btn_quit = Button(volet,text="Terminer",command=self.quitter)
        btn_quit.pack(side=RIGHT)

        self.lib = ScrollableFrame(self.fenetre,side=TOP,fill="both", expand=True)
        self.lib.show()
        if self.type == "Image":
            self.libImages()
        elif self.type == "Son":
            self.libSons()

        btn_addNewItem.bind("<1>",lambda event, lib="Lib"+self.type+"s" : self.addItem(event,lib))
        self.fenetre.mainloop()

    def addItem(self,event,lib):
        if lib.find("Image") != -1:
            filename = filedialog.askopenfilenames(filetypes=[("Image Files", ".png .jpg .jpeg")])
            filename = str(filename)
            filename = filename[2:-3]
            fn = str(pathlib.Path(__file__).parent)
            fn = fn [:-5] + "assets/" + lib
            fn = fn.replace("\\","/")
            copy(filename, fn)                                              
        else:
            filename = filedialog.askopenfilenames(filetypes=[("Sound Files", ".wav")])
            filename = str(filename)
            filename = filename[2:-3]
            fn = str(pathlib.Path(__file__).parent)
            fn = fn [:-5] + "assets/" + lib
            fn = fn.replace("\\","/")
            copy(filename, fn)     
        self.showItems(lib)
        

    def showItems(self,lib):
        if lib.find("Image") != -1:
            self.libImages()
        else:
            self.libSons()

    def libImages(self):
        self.clean(self.lib.getFrame())
        i = 0
        j = 0
        self.images = []
        self.photos = []
        self.items = []
        for pi in Bibliotheque.path.joinpath("assets/LibImages").iterdir():
            self.images.append(PIL.Image.open(pi))
            self.images[len(self.images)-1] = self.images[len(self.images)-1].resize((220,220), PIL.Image.ANTIALIAS)
            self.photos.append(PIL.ImageTk.PhotoImage(self.images[len(self.images)-1]))
            l = Img("Label",self.lib.getFrame(),menu=self.widget.getMenu(),text=str(pi),width=self.images[len(self.images)-1].width,height=self.images[len(self.images)-1].height, image = self.photos[len(self.photos)-1])
            self.items.append(l)
            self.items[len(self.items)-1].grid(row=i,column=j)
            self.items[len(self.items)-1].bind("<1>", self.on_selectedImage)
            j = j + 1
            if j == 5 :
                i = i + 1
                j = 0

    def on_selectedImage(self, event):
        x,y = event.widget.winfo_pointerxy()
        target = event.widget.winfo_containing(x,y)
        ind = self.items.index(target)
        self.widget["image"] = self.photos[ind]
        self.widget["width"] = 220
        self.widget["height"] = 220
        self.widget["text"] = self.items[ind]["text"]
        self.widget["font"] = self.items[ind]["font"]
        self.widget.image = self.photos[ind]
        if self.container != None :
            self.sess.update(self.container)
            self.clean(self.container.frame)
            self.container.showToolBox()
        else:
            self.sess.update(self.widget)

    def libSons(self):
        self.clean(self.lib.getFrame())
        i = 0
        j = 0
        self.son = []
        self.items = []
        for pi in Bibliotheque.path.joinpath("assets/LibSons").iterdir():
            s = Son("Button",self.lib.getFrame(),menu=self.widget.getMenu(),width=30,height=3)
            s.init(str(pi))
            s.setTexte(str(pi).split(".")[0].split("\\")[len(str(pi).split(".")[0].split("\\"))-1])
            self.son.append(str(pi))
            self.items.append(s)
            self.items[len(self.items)-1].grid(row=i,column=j)
            self.items[len(self.items)-1].bind("<1>", self.on_selectedSon)
            j = j + 1
            if j == 5 :
                i = i + 1
                j = 0

    def on_selectedSon(self,event):
        x,y = event.widget.winfo_pointerxy()
        target = event.widget.winfo_containing(x,y)
        ind = self.items.index(target)
        print(self.son[ind])
        self.widget.init(self.son[ind])
        self.widget.getSound().click()
        self.items[ind].getSound().click()
        if self.container != None :
            self.sess.update(self.container)
            self.clean(self.container.frame)
            self.container.showToolBox()
        else:
            self.sess.update(self.widget)

    def clean(self,frame):
        for child in frame.winfo_children():
            child.destroy()

    def quitter(self):
        self.fenetre.destroy()