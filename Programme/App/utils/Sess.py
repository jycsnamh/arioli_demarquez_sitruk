from tkinter import *
from services.Editor_services import Editor_services

class Sess :
    def __init__(self,root,token,id,nom,edit=True):
        self.token = token
        self.root = root
        self.idExercice = id
        self.nom = nom
        self.edit = edit
        self.services = Editor_services(self.token)
        if self.edit:
            self.idScript = str(len(self.services.getScriptsEdit())+1)
        else:
            self.idScript = str(len(self.services.getScriptsUse())+1)
        self.sess = {}
        
    
    # partie sess
    
    def initialisesession(self):
        #infos
        self.sess["idExercice"] = self.idExercice
        self.sess["nom"] = self.nom
        # script
        self.initScript()
    
    def findIn(self,src):
        return src in self.sess
    
    def getIdScript(self):
        return self.idScript
        
    def initScript(self):
        self.sess["script"] = "idScript="+self.idScript +" ; "

    def getScript(self):
        return self.sess["script"]
    
    def addToScript(self,str):
        self.sess["script"] = self.sess["script"] + str

    def add(self,strPathsess,WidgetFormated):
        pass

    def remove(self,idWidget):
        size =self.sess["script"][self.sess["script"].index("idWidget=" + str(idWidget)):len(self.sess["script"])].split(";!")[1]
        self.sess["script"] = self.sess["script"][0:self.sess["script"].index("idWidget=" + str(idWidget))] + \
                                            self.sess["script"][self.sess["script"].index("idWidget=" + str(idWidget))+int(size.split("=")[1]):]
        
    def update(self,widget):
        self.remove(widget.id)
        self.formate(widget)
        
    def formate(self, widget):
        fp ="idWidget=" + str(widget.id) + ";!" + "size="

        tp = ";!" +str(widget.__class__) + ";!"
        sc = ""
        if str(widget.__class__).find("ToolBox") == -1 :
            tp = tp + "Type=" + widget.type + ";!"
            for key in widget.configure():
                if key != 'master':
                    sc = sc + key + "=" + str(widget.cget(key)) + ";,"
            # Text
            if str(widget.__class__).find("Texte") != -1 :
                sc = sc + "text=" + widget.get("1.0",END)[0:len(widget.get("1.0",END))-1] + ";,"
                for tag in widget.tag :
                    sc = sc + tag + ";,"
                sc = sc + "active=" + widget.active + ";,"
            sc = sc + "x=" + str(widget.x) + ";,y=" + str(widget.y)+ ";," 
            if str(widget.__class__).find("Container") != -1 :
                sc = sc + "container="+ str(widget.getChild().__class__) + ";,"
            if str(widget.__class__).find("Son") != -1 :
                sc = sc + "fichier=" + str(widget.fichier) + ";,"
            if str(widget.getMenu().__class__).find("type") != -1 :
                sc = sc + "menu=" + str(widget.getMenu()) + ";,"
            else:
                sc = sc + "menu=" + str(widget.getMenu().__class__) + ";,"
            if str(widget.__class__).find("Bouton") != -1 :
                sc = sc + "actions=" + str(widget.getActions())
            elif str(widget.__class__).find("Container") != -1 or str(widget.__class__).find("Texte") != -1 :
                sc = sc + "resultatAttendu=" + str(widget.getResultatAttendu())
            else:
                sc = sc + "value=" + str(widget.getValue()) 
        else:
            sc = "children::"
            i = 0
            for child in widget.getChildren():
                sc = sc + "idChild=" + str(child.id) + ";/" +str(child.__class__) + ";/"
                sc = sc + "Type=" + child.type + ";/"
                for key in child.configure():
                    if key != 'master':
                        sc = sc + key + "=" + str(child.cget(key)) + ";?"
                if str(child.__class__).find("Son") != -1 :
                        sc = sc + "fichier=" + str(child.fichier) + ";?"
                # Text
                if str(child.__class__).find("Texte") != -1 :
                    sc = sc + "text=" + child.get("1.0",END)[0:len(child.get("1.0",END))-1] + ";?"
                    for tag in child.tag :
                        sc = sc + tag + ";,"
                    
                    if str(child.getMenu().__class__).find("type") != -1 :
                        sc = sc + "menu=" + str(child.getMenu()) + ";?"
                    else:
                        sc = sc + "menu=" + str(child.getMenu().__class__) + ";?"
                    sc = sc + "resultatAttendu=" + str(child.getResultatAttendu())
                else :
                    if str(child.getMenu().__class__).find("type") != -1 :
                        sc = sc + "menu=" + str(child.getMenu()) + ";?"
                    else:
                        sc = sc + "menu=" + str(child.getMenu().__class__) + ";?"
                    sc = sc + "value=" + str(child.getValue()) 
                if i < len(widget.getChildren())-1:
                    sc = sc + ";§"
                i = i + 1
        
        si = str(len(sc) + len(fp) + len(tp) + 2)
        scrpt = fp + str(int(si)+len(si))   + tp + sc
        scrpt = scrpt + ";$"
        self.addToScript(scrpt)
        

    def load(self,idExo):
        if self.edit:
            self.sess = self.services.loadEdit(idExo)
            self.sess["keyChamp"] = str(self.services.getKeyChampEdit(self.idExercice))
        else :
            self.sess = self.services.loadUse(idExo)
            self.sess["keyChamp"] = str(self.services.getKeyChampUse(self.idExercice))

    def save(self,event,info):
        b = self.services.save(self.sess)
        if b :
            info.affiche("Sauvegarde réussie.")
            
        else :
            info.affiche("Echec de la sauvegarde.")


    # partie composant

