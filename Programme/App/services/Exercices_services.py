import requests

class Exercices_services :
    def __init__(self,token):
        self.url="http://83.112.38.91:5000"
        self.token = token
        self.sess = requests.Session()
        self.sess.headers.update({'Authorization': '{access_token}'.format(access_token=self.token)})
        

    def getExercices(self):
        try:
            response = self.sess.get(self.url + "/Exercices/getExercicesUser")
            response.raise_for_status()
            return response.json()["exercices"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def creerExercice(self,nom,difficulte):
        try:
            response = self.sess.get(self.url + "/Exercices/creerExercice",params={"nom" : nom,"difficulte" : difficulte})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def supprimerExercice(self,idExercice):
        try:
            response = self.sess.delete(self.url + "/Exercices/deleteExercice", params={"idExercice" : idExercice})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
    
    def modifierDifficulteExercice(self,idExercice,difficulte):
        try:
            response = self.sess.put(self.url + "/Exercices/modifDifficulteExercice", params={"idExercice" : idExercice,"difficulte" : difficulte})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
    
    
    
    
    
    
    
    #eleves
    
    def load(self, idExo):
        try:
            response = self.sess.get(self.url + "/Editor/loadExercice?idExo="+ str(idExo))
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)


    