import requests

class Editor_services :
    def __init__(self,token):
        self.url="http://83.112.38.91:5000"
        self.token = token
        self.sess = requests.Session()
        self.sess.headers.update({'Authorization': '{access_token}'.format(access_token=self.token)})
        
    def loadEdit(self, idExo):
        try:
            response = self.sess.get(self.url + "/Editor/loadEdit?idExo="+ str(idExo))
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def loadUse(self, idExo):
        try:
            response = self.sess.get(self.url + "/Editor/loadUse?idExo="+ str(idExo))
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)


    def save(self, sess):
        try:
            response = self.sess.get(self.url + "/Editor/save", params={"script" : sess["script"], "keyChamp" : sess["keyChamp"]})
            response.raise_for_status()
            return response.json()["etat"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
        
    def getScriptsEdit(self):
        try:
            response = self.sess.get(self.url + "/Editor/getScriptsEdit")
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def getScriptsUse(self):
        try:
            response = self.sess.get(self.url + "/Editor/getScriptsUse")
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def getKeyChampEdit(self,idExercice):
        try:
            response = self.sess.get(self.url + "/Editor/getKeyChampExerciceEdit",params={"idExercice" : idExercice})
            response.raise_for_status()
            return response.json()["keyChamp"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def getKeyChampUse(self,idExercice):
        try:
            response = self.sess.get(self.url + "/Editor/getKeyChampExerciceUse",params={"idExercice" : idExercice})
            response.raise_for_status()
            return response.json()["keyChamp"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)