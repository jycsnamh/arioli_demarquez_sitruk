import requests

class Pages_services :
    def __init__(self,token):
        self.url="http://83.112.38.91:5000"
        self.token = token
        self.sess = requests.Session()
        self.sess.headers.update({'Authorization': '{access_token}'.format(access_token=self.token)})
        
    def creerPage(self,nom):
        try:
            response = self.sess.get(self.url + "/Pages/creerPage", params={"nom" : nom})
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
    
    def getPages(self):
        try:
            response = self.sess.get(self.url + "/Pages/getPagesUser")
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def supprimerPage(self,idPage):
        try:
            response = self.sess.delete(self.url + "/Pages/deletePage", params={"idPage" : idPage})
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)



    def addExercice(self,idPage,idExo,nomExo):
        try:
            response = self.sess.post(self.url + "/Pages/addExercice", params={"idPage": idPage,"idExercice" : idExo,"nom" : nomExo})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

        
    def removeExercice(self,idPage,idExo,nomExo):
        try:
            response = self.sess.delete(self.url + "/Pages/removeExercice", params={"idPage": idPage,"idExercice" : idExo,"nom" : nomExo})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def getExercices(self,idPage):
        try:
            response = self.sess.get(self.url + "/Pages/getExercices", params={"idPage": idPage})
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
