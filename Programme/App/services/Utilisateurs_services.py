import requests
import base64

class Utilisateurs_services :
    def __init__(self):
        self.url="http://83.112.38.91:5000"
        self.sess = requests.Session()
        #self.sess.headers.update({'Authorization': '{access_token}'.format(access_token=token)})

    def register(self,user):
        try :
            response = self.sess.get(self.url + "/Utilisateurs/registerUser",params=user)
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def login(self,pseudo,pwd):
        try:
            headers = {
                    'Authorization': 'Basic {}'.format(
                        base64.b64encode(
                            '{username}:{password}'.format(
                                username=pseudo,
                                password=pwd).encode()
                        ).decode()
                    )
                }
            response = self.sess.get(self.url + "/Utilisateurs/loginUser",headers=headers)
            response.raise_for_status()
            return response.json()["token"]
            
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)