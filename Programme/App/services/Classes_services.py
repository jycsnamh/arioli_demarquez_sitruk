import requests

class Classes_services :
    def __init__(self,token):
        self.url="http://83.112.38.91:5000"
        self.token = token
        self.sess = requests.Session()
        self.sess.headers.update({'Authorization': '{access_token}'.format(access_token=self.token)})
        
    def creerClasse(self,nom):
        try:
            response = self.sess.get(self.url + "/Classes/creerClasse", params={"nom" : nom})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)
    
    def getClasses(self):
        try:
            response = self.sess.get(self.url + "/Classes/getClasses")
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def supprimerClasse(self,idClasse):
        try:
            response = self.sess.delete(self.url + "/Classes/deleteClasse", params={"idClasse" : idClasse})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def addEleve(self,idClasse,nomEleve):
        try:
            response = self.sess.post(self.url + "/Classes/addEleve", params={"idClasse" : idClasse,"nomEleve" : nomEleve})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def deleteEleve(self,idClasse,nom):
        try:
            response = self.sess.delete(self.url + "/Classes/deleteEleve", params={"idClasse" : idClasse,"nom":nom})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def getEleves(self,idClasse):
        try:
            response = self.sess.get(self.url + "/Classes/getEleves", params={"idClasse" : idClasse})
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def genererCode(self,idClasse):
        try:
            response = self.sess.get(self.url + "/Classes/genererCode", params={"idClasse" : idClasse})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def getCode(self,idClasse):
        try:
            response = self.sess.get(self.url + "/Classes/getCode", params={"idClasse" : idClasse})
            response.raise_for_status()
            return response.json()["code"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def getPages(self,idClasse):
        try:
            response = self.sess.get(self.url + "/Classes/getPages", params={"idClasse": idClasse})
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def addPage(self,idClasse,idPage,nomPage):
        try:
            response = self.sess.post(self.url + "/Classes/addPage", params={"idClasse": idClasse,"idPage" : idPage,"nom" : nomPage})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def removePage(self,idClasse,idPage,nomPage):
        try:
            response = self.sess.delete(self.url + "/Classes/removePage", params={"idClasse": idClasse,"idPage" : idPage,"nom" : nomPage})
            response.raise_for_status()
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)