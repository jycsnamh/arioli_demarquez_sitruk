import requests
import base64

class Eleves_services :
    def __init__(self):
        self.url="http://83.112.38.91:5000"
        self.token = None
        self.sess = requests.Session()
        self.bUpdate = False 
        
    def updatesess(self):
        self.sess.headers.update({'Authorization': '{access_token}'.format(access_token=self.token)})
        self.bUpdate = True
        
    def register(self,user):
        try :
            response = self.sess.get(self.url + "/Eleves/registerEleve",params=user)
            return response.json()["message"]
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def login(self,prenom,code):
        try:
            headers = {
                    'Authorization': 'Basic {}'.format(
                        base64.b64encode(
                            '{username}:{password}'.format(
                                username=prenom,
                                password=code).encode()
                        ).decode()
                    )
                }
            response = self.sess.get(self.url + "/Eleves/loginEleve",headers=headers)
            response.raise_for_status()
            return response.json()
            
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)

    def getPagesToDo(self,token):
        try:
            self.token = token
            self.updatesess()
            response = self.sess.get(self.url + "/Eleves/getPagesToDo")
            response.raise_for_status()
            return response.json()
        except requests.exceptions.HTTPError as errh:
            print(errh)
        except requests.exceptions.ConnectionError as errc:
            print(errc)
        except requests.exceptions.Timeout as errt:
            print(errt)
        except requests.exceptions.RequestException as err:
            print(err)